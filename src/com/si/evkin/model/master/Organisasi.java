/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.master;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author TRI
 */
public class Organisasi implements Serializable{
    
    private long autoindex;
    private long indexPemerintahan;
    private String code;
    private String name;
    
    private List<Organisasi> listOrganisasi;
    
    public Organisasi(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getIndexPemerintahan() {
        return indexPemerintahan;
    }

    public void setIndexPemerintahan(long indexPemerintahan) {
        this.indexPemerintahan = indexPemerintahan;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Organisasi> getListOrganisasi() {
        return listOrganisasi;
    }

    public void setListOrganisasi(List<Organisasi> listOrganisasi) {
        this.listOrganisasi = listOrganisasi;
    }
    
    public String toString(){
        return code +" "+name;
    }
    
    
}
