/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.master;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class Kegiatan implements Serializable{
    
    private long autoindex;
    private long indexProgram;
    private String code;
    private String name; 
    
    private Program program;
    
    public Kegiatan(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getIndexProgram() {
        return indexProgram;
    }

    public void setIndexProgram(long indexProgram) {
        this.indexProgram = indexProgram;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }            
    
}
