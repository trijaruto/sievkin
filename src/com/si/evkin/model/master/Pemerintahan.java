package com.si.evkin.model.master;


import java.io.Serializable;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TRI
 */
public class Pemerintahan implements Serializable{
    
    private long autoindex;
    private long indexUrusan;
    private String code;
    private String name;   
    
    private List<Urusan> listUrusan;
    
    public Pemerintahan(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIndexUrusan() {
        return indexUrusan;
    }

    public void setIndexUrusan(long indexUrusan) {
        this.indexUrusan = indexUrusan;
    }

    public List<Urusan> getListUrusan() {
        return listUrusan;
    }

    public void setListUrusan(List<Urusan> listUrusan) {
        this.listUrusan = listUrusan;
    }
    
    public String toString(){
        return code +" "+ name;
    }
               
}
