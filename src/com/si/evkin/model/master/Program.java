/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.master;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class Program implements  Serializable{
    
    private long autoindex;
    private long indexOrganisasi;
    private String years;
    private String code;
    private String name;
    
    private Organisasi organisasi;
    
    public Program(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }   

    public long getIndexOrganisasi() {
        return indexOrganisasi;
    }

    public void setIndexOrganisasi(long indexOrganisasi) {
        this.indexOrganisasi = indexOrganisasi;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Organisasi getOrganisasi() {
        return organisasi;
    }

    public void setOrganisasi(Organisasi organisasi) {
        this.organisasi = organisasi;
    }
    
    public String toString(){
        return code +" "+ name;
    }
    
    
}
