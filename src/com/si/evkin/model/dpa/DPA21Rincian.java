/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.dpa;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class DPA21Rincian implements Serializable{
    
    private long autoindex;
    private long indexDPA1;
    private String uraian;
    private double jumlah;
    private String satuan;
    private double harga;
    
    public DPA21Rincian(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getIndexDPA1() {
        return indexDPA1;
    }

    public void setIndexDPA1(long indexDPA1) {
        this.indexDPA1 = indexDPA1;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }
    
    
    
    
    
    
}
