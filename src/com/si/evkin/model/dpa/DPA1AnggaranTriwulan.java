/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.dpa;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class DPA1AnggaranTriwulan implements Serializable{
    
    private long autoindex;
    private long indexDPA1;
    private double januari;
    private double februari;
    private double maret;
    private double april;
    private double mei;
    private double juni;
    private double juli;
    private double agustus;
    private double september;
    private double oktober;
    private double november;
    private double desember;
    
    public DPA1AnggaranTriwulan(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getIndexDPA1() {
        return indexDPA1;
    }

    public void setIndexDPA1(long indexDPA1) {
        this.indexDPA1 = indexDPA1;
    }

    public double getJanuari() {
        return januari;
    }

    public void setJanuari(double januari) {
        this.januari = januari;
    }

    public double getFebruari() {
        return februari;
    }

    public void setFebruari(double februari) {
        this.februari = februari;
    }

    public double getMaret() {
        return maret;
    }

    public void setMaret(double maret) {
        this.maret = maret;
    }

    public double getApril() {
        return april;
    }

    public void setApril(double april) {
        this.april = april;
    }

    public double getMei() {
        return mei;
    }

    public void setMei(double mei) {
        this.mei = mei;
    }

    public double getJuni() {
        return juni;
    }

    public void setJuni(double juni) {
        this.juni = juni;
    }

    public double getJuli() {
        return juli;
    }

    public void setJuli(double juli) {
        this.juli = juli;
    }

    public double getAgustus() {
        return agustus;
    }

    public void setAgustus(double agustus) {
        this.agustus = agustus;
    }

    public double getSeptember() {
        return september;
    }

    public void setSeptember(double september) {
        this.september = september;
    }

    public double getOktober() {
        return oktober;
    }

    public void setOktober(double oktober) {
        this.oktober = oktober;
    }

    public double getNovember() {
        return november;
    }

    public void setNovember(double november) {
        this.november = november;
    }

    public double getDesember() {
        return desember;
    }

    public void setDesember(double desember) {
        this.desember = desember;
    }
    
    
}
