/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.model.dpa;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author TRI
 */
public class DPA1 implements Serializable{
    
    private long autoindex;
    private String year;
    private long indexKodeRekening;
    private double jumlah;
    private String satuan;
    private double harga;
    
    private List<DPA1Rincian> listDPA1Rincian;
    
    public DPA1(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public long getIndexKodeRekening() {
        return indexKodeRekening;
    }

    public void setIndexKodeRekening(long indexKodeRekening) {
        this.indexKodeRekening = indexKodeRekening;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public List<DPA1Rincian> getListDPA1Rincian() {
        return listDPA1Rincian;
    }

    public void setListDPA1Rincian(List<DPA1Rincian> listDPA1Rincian) {
        this.listDPA1Rincian = listDPA1Rincian;
    }
    
    
    
}
