/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.si.evkin.model.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 *
 * @author anchii
 */
public class Terbilang {

    //    ============================== Script Terbilang ==============================
    private final String PEMBAGI_SEPULUH = "10";
    private final String PEMBAGI_SERATUS = "100";
    private final String PEMBAGI_SERIBU = "1000";
    private final String PEMBAGI_SEJUTA = "1000000";
    private final String PEMBAGI_SEMILYAR = "1000000000";
    private final String PEMBAGI_SETRILYUN = "1000000000000";


    public final String getTerbilangAll(BigDecimal dataUang, String mataUang) {
        String sValue;
        String jenis;
        String sTemp;//String.valueOf(dataUang);

        if(dataUang.compareTo(BigDecimal.ZERO) < 0) {
            jenis = "Minus";
            sTemp = getDecimalFormat(dataUang.multiply(BigDecimal.ONE.negate()));
        } else {
            jenis = "";
            sTemp = getDecimalFormat(dataUang);
        }
        System.out.println("Bilangan : " + jenis);
        System.out.println("sTemp : Rp." + sTemp);
        String[] sTemp1 = sTemp.split("\\.");
        System.out.println("Lenght sTemp1 : " + sTemp1.length);

        String s1 = sTemp1[0];
        System.out.println("s1 : " + s1);
        if (s1.equals("0")) {
            s1 = "Nol";
        } else {
//            System.out.println("Nilai s1 : Rp." + sTemp);
            s1 = getTerbilang(new BigDecimal(Double.parseDouble(sTemp1[0])));
        }

        String koma = " Koma";
        String space = " ";
        String s2 = sTemp1[1];
//        System.out.println("s2 : " + s2);
        if (!s2.equals("00")) {
            System.out.println("s2 => " + s2);

//            String x = getTerbilangKoma(s2);
            String x = getTerbilang(new BigDecimal(Double.parseDouble(s2)));
            sValue = s1 + space + mataUang + x + space + "Sen";
        } else {
            sValue = s1 + space + mataUang;
        }

        return jenis + sValue;
    }//akhir dari method getTerbilangAll

    private String getTerbilangKoma(String data) {
        String result = new String();
        int size_STR;
        size_STR = data.length();
        if(size_STR == 1) {
            result = getSatuanKoma(Integer.parseInt(data));
        }
        else if(size_STR == 2) {
            String s1 = data.substring(0, 1);
            String s2 = data.substring(1, 2);
            if(!"0".equals(s2)){
                s1 = getSatuanKoma(Integer.parseInt(s1));
                s2 = getSatuanKoma(Integer.parseInt(s2));
                result = s1 + s2;
            } else {
                result = getSatuanKoma(Integer.parseInt(s1));
            }
        }
        return result;
    }//akhir dari method getTerbilangKoma

    // ambil terbilang untuk satuan Koma
    private String getSatuanKoma(int data) {
        switch (data) {
            case 0: return " Nol";
            case 1: return " Satu";
            case 2: return " Dua";
            case 3: return " Tiga";
            case 4: return " Empat";
            case 5: return " Lima";
            case 6: return " Enam";
            case 7: return " Tujuh";
            case 8: return " Delapan";
            case 9: return " Sembilan";
            default: return " ";
        }
    }//akhir dari method getSatuanKoma


    public String getTerbilang(BigDecimal dataUang) {
        final String CONST_SPASI = " ";
        final String CONST_NOL = "0";
        final String CONST_11 = "11";
        final String CONST_19 = "19";
        final String CONST_99 = "99";
        final String CONST_199 = "199";
        final String CONST_999 = "999";
        final String CONST_1999 = "1999";
        final String CONST_999_RIBU = "999999";
        final String CONST_999_JUTA = "999999999";
        final String CONST_999_MILYAR = "999999999999";
        final String CONST_999_TRILYUN = "999999999999999";
        StringBuilder result = new StringBuilder();
        int satuan;

        // cek apakah data minus
        if (dataUang.compareTo(BigDecimal.ZERO) < 0) {
            dataUang = dataUang.multiply(BigDecimal.ONE.negate());
            result.append("Minus");

        }

        // data dianggap tidak minus
        if (cekData(dataUang, CONST_NOL, CONST_11)) {
            result.append(CONST_SPASI)
                    .append(getSatuan(dataUang.intValue()));
        } else if (cekData(dataUang, CONST_11, CONST_19)) {
            result.append(getTerbilang(mod(dataUang, PEMBAGI_SEPULUH)))
                    .append(CONST_SPASI)
                    .append("Belas");
        } else if (cekData(dataUang, CONST_19, CONST_99)) {
            result.append(getTerbilang(div(dataUang, PEMBAGI_SEPULUH)))
                    .append(CONST_SPASI)
                    .append("Puluh")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SEPULUH)));
        } else if (cekData(dataUang, CONST_99, CONST_199)) {
            result.append(CONST_SPASI)
                    .append("Seratus")
                    .append(getTerbilang(dataUang.subtract(new BigDecimal(PEMBAGI_SERATUS))));
        } else if (cekData(dataUang, CONST_199, CONST_999)) {
            result.append(getTerbilang(div(dataUang, PEMBAGI_SERATUS)))
                    .append(CONST_SPASI)
                    .append("Ratus")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SERATUS)));
        } else if (cekData(dataUang, CONST_999, CONST_1999)) {
            result.append(CONST_SPASI)
                    .append("Seribu")
                    .append(getTerbilang(dataUang.subtract(new BigDecimal(PEMBAGI_SERIBU))));
        } else if (cekData(dataUang, CONST_1999, CONST_999_RIBU)) {
            result.append(getTerbilang(div(dataUang, PEMBAGI_SERIBU)))
                    .append(CONST_SPASI)
                    .append("Ribu")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SERIBU)));
        } else if (cekData(dataUang, CONST_999_RIBU, CONST_999_JUTA)) {
            result.append(getTerbilang(div(dataUang, PEMBAGI_SEJUTA)))
                    .append(CONST_SPASI)
                    .append("Juta")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SEJUTA)));
        }else if (cekData(dataUang, CONST_999_JUTA, CONST_999_MILYAR)){
            result.append(getTerbilang(div(dataUang, PEMBAGI_SEMILYAR)))
                    .append(CONST_SPASI)
                    .append("Milyar")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SEMILYAR)));
        }else if (cekData(dataUang, CONST_999_MILYAR, CONST_999_TRILYUN)){
            result.append(getTerbilang(div(dataUang, PEMBAGI_SETRILYUN)))
                    .append(CONST_SPASI)
                    .append("Trilyun")
                    .append(getTerbilang(mod(dataUang, PEMBAGI_SETRILYUN)));
        }

        return result.toString();
    }//akhir dari method getTerbilang

    // ambil terbilang untuk satuan
    private String getSatuan(int data) {
        switch (data) {
            case 1: return "Satu";
            case 2: return "Dua";
            case 3: return "Tiga";
            case 4: return "Empat";
            case 5: return "Lima";
            case 6: return "Enam";
            case 7: return "Tujuh";
            case 8: return "Delapan";
            case 9: return "Sembilan";
            case 10: return "Sepuluh";
            case 11: return "Sebelas";
            default: return " ";
        }
    }//akhir dari method getSatuan


    // cek apakah data memenuhi syarat
    private boolean cekData(BigDecimal dataPembanding, String strBatasBawah, String strBatasAtas) {
        BigDecimal batasBawah = new BigDecimal(strBatasBawah);
        BigDecimal batasAtas = new BigDecimal(strBatasAtas);
        if (dataPembanding.compareTo(batasBawah) > 0) {
            if (dataPembanding.compareTo(batasAtas) <= 0) {
                return true;
            }
        }
        return false;
    }//akhir dari method cekData

    // mengambil sisa hasil bagi
    private BigDecimal mod(BigDecimal data, String strPembagi) {
        BigDecimal pembagi = new BigDecimal(strPembagi);
        return data.remainder(pembagi);
    }//akhir dari method mod

    // mengambil hasil bagi dengan membuang angka dibelakang koma
    private BigDecimal div(BigDecimal data, String strPembagi) {
        BigDecimal pembagi = new BigDecimal(strPembagi);
        return data.divide(pembagi, 1);
    }//akhir dari method div

    private static String getDecimalFormat(BigDecimal value){
        DecimalFormatSymbols symbol = new DecimalFormatSymbols();
        symbol.setDecimalSeparator('.');
//        symbol.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("####0.00;(####0.00)", symbol);
        return df.format(value);
    }
}