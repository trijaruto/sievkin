/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.si.evkin.model.util;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Bunxu
 */
public class AppYears implements Serializable{

    public AppYears() {
    }        

    public static List<String> initYears() {
        List<String> year = new ArrayList<>();
        try {
            int startY = Integer.parseInt("2012");
            java.util.Date date = new java.util.Date();
            java.util.GregorianCalendar cal = getCalendar();
            cal.setTime(date);
            cal.set(GregorianCalendar.YEAR, cal.get(GregorianCalendar.YEAR) + 3);
            int endY = cal.get(GregorianCalendar.YEAR);
            for (int i = startY; i <= endY; i++) {
                year.add(String.valueOf(i));
            }
        } catch (Exception ex) {
        }
        return year;
    }

    public static GregorianCalendar getCalendar() {
        String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);

        if (ids.length == 0) {
            System.exit(0);
        }
        SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
        GregorianCalendar calendar = new GregorianCalendar(pdt);
        Date date = new Date();
        calendar.setTime(date);
        return calendar;
    }
}
