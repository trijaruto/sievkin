/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.ctrl.sql;

import com.si.evkin.model.master.Kegiatan;
import com.si.evkin.model.master.Organisasi;
import com.si.evkin.model.master.Pegawai;
import com.si.evkin.model.master.Pemerintahan;
import com.si.evkin.model.master.Program;
import com.si.evkin.model.master.Urusan;
import java.sql.Connection;
import java.util.List;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public interface IEvkin {
    
    /*MASTER*/
    
    //PEGAWAI
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws Exception;   
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception;
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception;            
    public DefaultListModel onLoadPegawai(Connection conn) throws Exception;   
    
    //URUSAN
    public long onSaveMasterUrusan(Urusan urusan, Connection conn) throws Exception;   
    public void onUpdateMasterUrusan(Urusan urusan, Connection conn) throws Exception;
    public void onDeleteMasterUrusanByAutoindex(long autoindex, Connection conn) throws Exception;  
    public Urusan getUrusanByAutoindex(long autoindex, Connection conn) throws Exception;
    public List<Urusan> onLoadUrusan(Connection conn) throws Exception;    
    
    
    //PEMERINTAHAN
    public long onSaveMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception;   
    public void onUpdateMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception;
    public void onDeleteMasterPemerintahanByAutoindex(long autoindex, Connection conn) throws Exception;  
    public Pemerintahan getPemerintahanByAutoindex(long autoindex, Connection conn) throws Exception;
    public List<Pemerintahan> onLoadPemerintahan(Connection conn) throws Exception;
    public List<Pemerintahan> onLoadPemerintahanByIndexUrusan(long autoindex, Connection conn) throws Exception;
    
    
    //PEMERINTAHAN
    public long onSaveMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception;   
    public void onUpdateMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception;
    public void onDeleteMasterOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception;  
    public Organisasi getOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception;
    public List<Organisasi> onLoadOrganisasi(Connection conn) throws Exception;
    public List<Organisasi> onLoadOrganisasiByIndexPemerintahan(long autoindex, Connection conn) throws Exception;
    
    //PROGRAM
    public long onSaveMasterProgram(Program program, Connection conn) throws Exception;   
    public void onUpdateMasterProgram(Program program, Connection conn) throws Exception;
    public void onDeleteMasterProgramByAutoindex(long autoindex, Connection conn) throws Exception;  
    public Program getProgramByAutoindex(long autoindex, Connection conn) throws Exception;
    public List<Program> onLoadProgram(Connection conn) throws Exception;
    public List<Program> onLoadProgramByIndexOrganisasi(long autoindex, Connection conn) throws Exception;
    public List<Program> onLoadProgramByIndexOrganisasiAndYear(long autoindex, String years, Connection conn) throws Exception;
    public List<Program> onLoadProgramByYears(String years, Connection conn) throws Exception;
    
    //KEGIATAN
    public long onSaveMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception;   
    public void onUpdateMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception;
    public void onDeleteMasterKegiatanByAutoindex(long autoindex, Connection conn) throws Exception;  
    public Kegiatan getKegiatanByAutoindex(long autoindex, Connection conn) throws Exception;
    public List<Kegiatan> onLoadKegiatan(Connection conn) throws Exception;
    public List<Kegiatan> onLoadKegiatanByIndexProgram(long autoindex, Connection conn) throws Exception;
    public List<Kegiatan> onLoadKegiatanByIndexProgramAndYear(long autoindex, String years, Connection conn) throws Exception;
    
}
