/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.ctrl.sql;

import com.si.evkin.model.master.Kegiatan;
import com.si.evkin.model.master.Organisasi;
import com.si.evkin.model.master.Pegawai;
import com.si.evkin.model.master.Pemerintahan;
import com.si.evkin.model.master.Program;
import com.si.evkin.model.master.Urusan;
import java.sql.Connection;
import java.util.List;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public class CtrlEvkin implements IEvkin{

    private long indexReturn = 0;

    @Override
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterPegawai(pegawai, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterPegawai\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterPegawai(pegawai, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterPegawai\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterPegawaiByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterPegawaiByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public DefaultListModel onLoadPegawai(Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadPegawai(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadPegawai\n" + ex.toString());           
        }
    }

    @Override
    public long onSaveMasterUrusan(Urusan urusan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterUrusan(urusan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterUrusan\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterUrusan(Urusan urusan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterUrusan(urusan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterUrusan\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterUrusanByAutoindex(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterUrusanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterUrusanByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Urusan> onLoadUrusan(Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadUrusan(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadUrusan\n" + ex.toString());           
        }
    }

    @Override
    public Urusan getUrusanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.getUrusanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal getUrusanByAutoindex\n" + ex.toString());           
        }
    }
    
    
    
    @Override
    public long onSaveMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterPemerintahan(pemerintahan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterPemerintahan\n" + ex.toString());           
        }
    }
    
  
    @Override
    public void onUpdateMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterPemerintahan(pemerintahan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterPemerintahan\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterPemerintahanByAutoindex(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterPemerintahanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterPemerintahanByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Pemerintahan> onLoadPemerintahan(Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadPemerintahan(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadPemerintahan\n" + ex.toString());           
        }
    }

    @Override
    public Pemerintahan getPemerintahanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.getPemerintahanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal getPemerintahanByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Pemerintahan> onLoadPemerintahanByIndexUrusan(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadPemerintahanByIndexUrusan(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadPemerintahanByIndexUrusan\n" + ex.toString());           
        }
    }

    @Override
    public long onSaveMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception {
         try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterOrganisasi(organisasi, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterOrganisasi\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterOrganisasi(organisasi, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterOrganisasi\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterOrganisasiByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterOrganisasiByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public Organisasi getOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.getOrganisasiByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal getOrganisasiByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Organisasi> onLoadOrganisasi(Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadOrganisasi(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadOrganisasi\n" + ex.toString());           
        }
    }
    
    @Override
    public List<Organisasi> onLoadOrganisasiByIndexPemerintahan(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadOrganisasiByIndexPemerintahan(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadOrganisasiByIndexPemerintahan\n" + ex.toString());           
        }
    }

    @Override
    public long onSaveMasterProgram(Program program, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterProgram(program, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterProgram\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterProgram(Program program, Connection conn) throws Exception {
         try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterProgram(program, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterProgram\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterProgramByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterProgramByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterProgramByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public Program getProgramByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.getProgramByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal getProgramByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Program> onLoadProgram(Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadProgram(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadProgram\n" + ex.toString());           
        }
    }

    @Override
    public List<Program> onLoadProgramByIndexOrganisasi(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadProgramByIndexOrganisasi(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadProgramByIndexOrganisasi\n" + ex.toString());           
        }
    }

    @Override
    public List<Program> onLoadProgramByIndexOrganisasiAndYear(long autoindex, String years, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadProgramByIndexOrganisasiAndYear(autoindex, years, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadProgramByIndexOrganisasiAndYear\n" + ex.toString());           
        }
    }
    
    @Override
    public List<Program> onLoadProgramByYears(String years, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadProgramByYears(years, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadProgramByYears\n" + ex.toString());           
        }
    }

    @Override
    public long onSaveMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            indexReturn = sql.onSaveMasterKegiatan(kegiatan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return indexReturn;
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onSaveMasterKegiatan\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            EvkinSQL sql = new EvkinSQL();
            sql.onUpdateMasterKegiatan(kegiatan, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onUpdateMasterProgram\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterKegiatanByAutoindex(long autoindex, Connection conn) throws Exception {
         try {           
            EvkinSQL sql = new EvkinSQL();            
            sql.onDeleteMasterKegiatanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onDeleteMasterKegiatanByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public Kegiatan getKegiatanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.getKegiatanByAutoindex(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal getKegiatanByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public List<Kegiatan> onLoadKegiatan(Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadKegiatan(conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadKegiatan\n" + ex.toString());           
        }
    }

    @Override
    public List<Kegiatan> onLoadKegiatanByIndexProgram(long autoindex, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadKegiatanByIndexProgram(autoindex, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadKegiatanByIndexProgram\n" + ex.toString());           
        }
    }

    @Override
    public List<Kegiatan> onLoadKegiatanByIndexProgramAndYear(long autoindex, String years, Connection conn) throws Exception {
        try {           
            EvkinSQL sql = new EvkinSQL();            
            return sql.onLoadKegiatanByIndexProgramAndYear(autoindex, years, conn);
        } catch (Exception ex) {           
            throw new Exception("CtrlEvkin : Gagal onLoadKegiatanByIndexProgramAndYear\n" + ex.toString());           
        }
    }

    public long getIndexReturn() {
        return indexReturn;
    }

    public void setIndexReturn(long indexReturn) {
        this.indexReturn = indexReturn;
    }

    
    

    
}
