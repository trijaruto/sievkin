/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.ctrl.sql;

import com.si.evkin.model.master.Kegiatan;
import com.si.evkin.model.master.Organisasi;
import com.si.evkin.model.master.Pegawai;
import com.si.evkin.model.master.Pemerintahan;
import com.si.evkin.model.master.Program;
import com.si.evkin.model.master.Urusan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public class EvkinSQL implements IEvkin{
    
    private PreparedStatement pstm = null;
    private Statement stm = null;
    private ResultSet rs = null;
    private long autoindex = 0;
            
    public EvkinSQL(){
        
    }
    
    public long getMaxIndex(String tablename, Connection conn) throws SQLException {        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT MAX(autoindex) as maxindex FROM " + tablename);
            rs.next();
            return rs.getLong("maxindex");
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws SQLException {                   
        try {
            pstm = conn.prepareStatement("INSERT INTO pegawai( " +
                        " nip, nama, jabatan, eselon, golongan) " +
                        " VALUES (?, ?, ?, ?, ?);");
            
            int col = 1;            
            pstm.setString(col++, pegawai.getNip());
            pstm.setString(col++, pegawai.getName());
            pstm.setString(col++, pegawai.getJabatan());
            pstm.setString(col++, pegawai.getEselon());
            pstm.setString(col++, pegawai.getGolongan());

            pstm.execute();
            
            autoindex = getMaxIndex("pegawai", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }
    
    @Override
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE pegawai "
                    + "SET nip=?, nama=?, jabatan=?, eselon=?, golongan=?"
                    + "WHERE autoindex = "+pegawai.getAutoindex());
            
            int col = 1;            
            pstm.setString(col++, pegawai.getNip());
            pstm.setString(col++, pegawai.getName());
            pstm.setString(col++, pegawai.getJabatan());
            pstm.setString(col++, pegawai.getEselon());
            pstm.setString(col++, pegawai.getGolongan());

            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }       
    }

    @Override
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM pegawai "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public DefaultListModel onLoadPegawai(Connection conn) throws SQLException {        
        DefaultListModel obj = new DefaultListModel();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM pegawai;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Pegawai pegawai = new Pegawai();
                pegawai.setAutoindex(rs.getLong("autoindex"));
                pegawai.setNip(rs.getString("nip"));
                pegawai.setName(rs.getString("nama"));
                pegawai.setJabatan(rs.getString("jabatan"));
                pegawai.setGolongan(rs.getString("eselon"));
                pegawai.setEselon(rs.getString("golongan"));                
                obj.addElement(pegawai);
            }

            return obj;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }        
    }
    
    
    @Override
    public long onSaveMasterUrusan(Urusan urusan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("INSERT INTO urusan( " +
                        " code, name) " +
                        " VALUES (?, ?);");
            
            int col = 1;            
            pstm.setString(col++, urusan.getCode());
            pstm.setString(col++, urusan.getName());
           

            pstm.execute();
            
            autoindex = getMaxIndex("urusan", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }

    @Override
    public void onUpdateMasterUrusan(Urusan urusan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE urusan "
                    + "SET code=?, name=? "
                    + "WHERE autoindex = "+urusan.getAutoindex());
            
            int col = 1;            
            pstm.setString(col++, urusan.getCode());
            pstm.setString(col++, urusan.getName());
           
            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }  
    }

    @Override
    public void onDeleteMasterUrusanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM urusan "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<Urusan> onLoadUrusan(Connection conn) throws Exception {
        List<Urusan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM urusan;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Urusan urusan = new Urusan();
                urusan.setAutoindex(rs.getLong("autoindex"));
                urusan.setCode(rs.getString("code"));
                urusan.setName(rs.getString("name"));
                               
                data.add(urusan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }  
    }
    
    @Override
    public Urusan getUrusanByAutoindex(long autoindex, Connection conn) throws SQLException {
        Urusan urusan = null;
        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM urusan WHERE autoindex = "+autoindex+";";

            rs = stm.executeQuery(sql);
            if (rs.next()) {
                urusan = new Urusan();
                urusan.setAutoindex(rs.getLong("autoindex"));
                urusan.setCode(rs.getString("code"));
                urusan.setName(rs.getString("name"));                   
            }

            return urusan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    
    
    @Override
    public long onSaveMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("INSERT INTO pemerintahan( " +
                        " indexurusan, code, name) " +
                        " VALUES (?, ?, ?);");
            
            int col = 1;     
            pstm.setLong(col++, pemerintahan.getIndexUrusan());
            pstm.setString(col++, pemerintahan.getCode());
            pstm.setString(col++, pemerintahan.getName());
           

            pstm.execute();
            
            autoindex = getMaxIndex("pemerintahan", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }

    @Override
    public void onUpdateMasterPemerintahan(Pemerintahan pemerintahan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE pemerintahan "
                    + "SET indexurusan=?, code=?, name=? "
                    + "WHERE autoindex = "+pemerintahan.getAutoindex());
            
            int col = 1;            
            pstm.setLong(col++, pemerintahan.getIndexUrusan());
            pstm.setString(col++, pemerintahan.getCode());
            pstm.setString(col++, pemerintahan.getName());
           
            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }  
    }

    @Override
    public void onDeleteMasterPemerintahanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM pemerintahan "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<Pemerintahan> onLoadPemerintahan(Connection conn) throws Exception {
        List<Pemerintahan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM pemerintahan;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Pemerintahan pemerintahan = new Pemerintahan();
                pemerintahan.setAutoindex(rs.getLong("autoindex"));
                pemerintahan.setIndexUrusan(rs.getLong("indexurusan"));
                pemerintahan.setCode(rs.getString("code"));
                pemerintahan.setName(rs.getString("name"));
                               
                data.add(pemerintahan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }  
    }
    
    @Override
    public Pemerintahan getPemerintahanByAutoindex(long autoindex, Connection conn) throws SQLException {
        Pemerintahan pemerintahan = null;
        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM pemerintahan WHERE autoindex = "+autoindex+";";

            rs = stm.executeQuery(sql);
            if (rs.next()) {
                pemerintahan = new Pemerintahan();
                pemerintahan.setAutoindex(rs.getLong("autoindex"));
                pemerintahan.setIndexUrusan(rs.getLong("indexurusan"));
                pemerintahan.setCode(rs.getString("code"));
                pemerintahan.setName(rs.getString("name"));                   
            }

            return pemerintahan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public List<Pemerintahan> onLoadPemerintahanByIndexUrusan(long autoindex, Connection conn) throws SQLException {
        List<Pemerintahan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM pemerintahan WHERE indexurusan = "+autoindex+";";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Pemerintahan pemerintahan = new Pemerintahan();
                pemerintahan.setAutoindex(rs.getLong("autoindex"));
                pemerintahan.setIndexUrusan(rs.getLong("indexurusan"));
                pemerintahan.setCode(rs.getString("code"));
                pemerintahan.setName(rs.getString("name"));                               
                data.add(pemerintahan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }  
    }
    
    
    @Override
    public long onSaveMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("INSERT INTO organisasi( " +
                        " indexpemerintahan, code, name) " +
                        " VALUES (?, ?, ?);");
            
            int col = 1;     
            pstm.setLong(col++, organisasi.getIndexPemerintahan());
            pstm.setString(col++, organisasi.getCode());
            pstm.setString(col++, organisasi.getName());
           

            pstm.execute();
            
            autoindex = getMaxIndex("organisasi", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }

    @Override
    public void onUpdateMasterOrganisasi(Organisasi organisasi, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE organisasi "
                    + "SET indexpemerintahan=?, code=?, name=? "
                    + "WHERE autoindex = "+organisasi.getAutoindex());
            
            int col = 1;            
            pstm.setLong(col++, organisasi.getIndexPemerintahan());
            pstm.setString(col++, organisasi.getCode());
            pstm.setString(col++, organisasi.getName());
           
            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }  
    }

    @Override
    public void onDeleteMasterOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM organisasi "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public Organisasi getOrganisasiByAutoindex(long autoindex, Connection conn) throws Exception {
        Organisasi organisasi = null;
        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM organisasi WHERE autoindex = "+autoindex+";";

            rs = stm.executeQuery(sql);
            if (rs.next()) {
                organisasi = new Organisasi();
                organisasi.setAutoindex(rs.getLong("autoindex"));
                organisasi.setIndexPemerintahan(rs.getLong("indexpemerintahan"));
                organisasi.setCode(rs.getString("code"));
                organisasi.setName(rs.getString("name"));                   
            }

            return organisasi;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<Organisasi> onLoadOrganisasi(Connection conn) throws Exception {
        List<Organisasi> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM organisasi;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Organisasi organisasi = new Organisasi();
                organisasi.setAutoindex(rs.getLong("autoindex"));
                organisasi.setIndexPemerintahan(rs.getLong("indexpemerintahan"));
                organisasi.setCode(rs.getString("code"));
                organisasi.setName(rs.getString("name"));
                               
                data.add(organisasi);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    @Override
    public List<Organisasi> onLoadOrganisasiByIndexPemerintahan(long autoindex, Connection conn) throws Exception {
        List<Organisasi> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM organisasi WHERE indexpemerintahan = "+autoindex+";";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Organisasi organisasi = new Organisasi();
                organisasi.setAutoindex(rs.getLong("autoindex"));
                organisasi.setIndexPemerintahan(rs.getLong("indexpemerintahan"));
                organisasi.setCode(rs.getString("code"));
                organisasi.setName(rs.getString("name"));                               
                data.add(organisasi);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }
    
    
    @Override
    public long onSaveMasterProgram(Program program, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("INSERT INTO program( " +
                        " indexorganisasi, years, code, name) " +
                        " VALUES (?, ?, ?, ?);");
            
            int col = 1;     
            pstm.setLong(col++, program.getIndexOrganisasi());
            pstm.setString(col++, program.getYears());
            pstm.setString(col++, program.getCode());
            pstm.setString(col++, program.getName());
           

            pstm.execute();
            
            autoindex = getMaxIndex("program", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }

    @Override
    public void onUpdateMasterProgram(Program program, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE program "
                    + "SET indexorganisasi=?, years,=?, code=?, name=? "
                    + "WHERE autoindex = "+program.getAutoindex());
            
            int col = 1;            
            pstm.setLong(col++, program.getIndexOrganisasi());
            pstm.setString(col++, program.getYears());
            pstm.setString(col++, program.getCode());
            pstm.setString(col++, program.getName());
           
            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }  
    }

    @Override
    public void onDeleteMasterProgramByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM program "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public Program getProgramByAutoindex(long autoindex, Connection conn) throws Exception {
        Program program = null;
        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM program WHERE autoindex = "+autoindex+";";

            rs = stm.executeQuery(sql);
            if (rs.next()) {
                program = new Program();
                program.setAutoindex(rs.getLong("autoindex"));
                program.setIndexOrganisasi(rs.getLong("indexorganisasi"));
                program.setYears(rs.getString("years"));
                program.setCode(rs.getString("code"));
                program.setName(rs.getString("name"));                   
            }

            return program;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<Program> onLoadProgram(Connection conn) throws Exception {
        List<Program> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM program;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Program program = new Program();
                program.setAutoindex(rs.getLong("autoindex"));
                program.setIndexOrganisasi(rs.getLong("indexorganisasi"));
                program.setYears(rs.getString("years"));
                program.setCode(rs.getString("code"));
                program.setName(rs.getString("name")); 
                               
                data.add(program);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    @Override
    public List<Program> onLoadProgramByIndexOrganisasi(long autoindex, Connection conn) throws Exception {
         List<Program> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM program WHERE indexorganisasi = "+autoindex+";";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Program program = new Program();
                program.setAutoindex(rs.getLong("autoindex"));
                program.setIndexOrganisasi(rs.getLong("indexorganisasi"));
                program.setYears(rs.getString("years"));
                program.setCode(rs.getString("code"));
                program.setName(rs.getString("name")); 
                               
                data.add(program);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    @Override
    public List<Program> onLoadProgramByIndexOrganisasiAndYear(long autoindex, String years, Connection conn) throws Exception {
        List<Program> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM program WHERE indexorganisasi = "+autoindex+" AND years = '"+years+"';";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Program program = new Program();
                program.setAutoindex(rs.getLong("autoindex"));
                program.setIndexOrganisasi(rs.getLong("indexorganisasi"));
                program.setYears(rs.getString("years"));
                program.setCode(rs.getString("code"));
                program.setName(rs.getString("name")); 
                               
                data.add(program);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public List<Program> onLoadProgramByYears(String years, Connection conn) throws Exception {
        List<Program> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM program WHERE years = '"+years+"';";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Program program = new Program();
                program.setAutoindex(rs.getLong("autoindex"));
                program.setIndexOrganisasi(rs.getLong("indexorganisasi"));
                program.setYears(rs.getString("years"));
                program.setCode(rs.getString("code"));
                program.setName(rs.getString("name")); 
                               
                data.add(program);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    

    public PreparedStatement getPstm() {
        return pstm;
    }

    public void setPstm(PreparedStatement pstm) {
        this.pstm = pstm;
    }

    public Statement getStm() {
        return stm;
    }

    public void setStm(Statement stm) {
        this.stm = stm;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }



    @Override
    public long onSaveMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("INSERT INTO kegiatan( " +
                        " indexprogram, code, name) " +
                        " VALUES (?, ?, ?);");
            
            int col = 1;     
            pstm.setLong(col++, kegiatan.getIndexProgram());            
            pstm.setString(col++, kegiatan.getCode());
            pstm.setString(col++, kegiatan.getName());
           

            pstm.execute();
            
            autoindex = getMaxIndex("kegiatan", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }

    @Override
    public void onUpdateMasterKegiatan(Kegiatan kegiatan, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE kegiatan "
                    + "SET indexprogram=?, code=?, name=? "
                    + "WHERE autoindex = "+kegiatan.getAutoindex());
            
            int col = 1;            
            pstm.setLong(col++, kegiatan.getIndexProgram());            
            pstm.setString(col++, kegiatan.getCode());
            pstm.setString(col++, kegiatan.getName());
           
            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }  
    }

    @Override
    public void onDeleteMasterKegiatanByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM kegiatan "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public Kegiatan getKegiatanByAutoindex(long autoindex, Connection conn) throws Exception {
        Kegiatan kegiatan = null;
        try {
            stm = conn.createStatement();

            String sql = "SELECT * FROM kegiatan WHERE autoindex = "+autoindex+";";

            rs = stm.executeQuery(sql);
            if (rs.next()) {
                kegiatan = new Kegiatan();
                kegiatan.setAutoindex(rs.getLong("autoindex"));
                kegiatan.setIndexProgram(rs.getLong("indexprogram"));             
                kegiatan.setCode(rs.getString("code"));
                kegiatan.setName(rs.getString("name"));                   
            }

            return kegiatan;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }
    }

    @Override
    public List<Kegiatan> onLoadKegiatan(Connection conn) throws Exception {
        List<Kegiatan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM kegiatan;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Kegiatan kegiatan = new Kegiatan();
                kegiatan.setAutoindex(rs.getLong("autoindex"));
                kegiatan.setIndexProgram(rs.getLong("indexprogram"));                
                kegiatan.setCode(rs.getString("code"));
                kegiatan.setName(rs.getString("name")); 
                               
                data.add(kegiatan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    @Override
    public List<Kegiatan> onLoadKegiatanByIndexProgram(long autoindex, Connection conn) throws Exception {
        List<Kegiatan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM kegiatan WHERE indexprogram = "+autoindex+";";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Kegiatan kegiatan = new Kegiatan();
                kegiatan.setAutoindex(rs.getLong("autoindex"));
                kegiatan.setIndexProgram(rs.getLong("indexprogram"));                
                kegiatan.setCode(rs.getString("code"));
                kegiatan.setName(rs.getString("name")); 
                               
                data.add(kegiatan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    @Override
    public List<Kegiatan> onLoadKegiatanByIndexProgramAndYear(long autoindex, String years, Connection conn) throws Exception {
        List<Kegiatan> data = new ArrayList<>();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM kegiatan "
                    + "INNER JOIN Program program on program.autoindex=kegiatan.indexprogram "
                    + "WHERE kegiatan.indexprogram = "+autoindex+" AND program.years = '"+years+"';";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Kegiatan kegiatan = new Kegiatan();
                kegiatan.setAutoindex(rs.getLong("autoindex"));
                kegiatan.setIndexProgram(rs.getLong("indexprogram"));                
                kegiatan.setCode(rs.getString("code"));
                kegiatan.setName(rs.getString("name")); 
                               
                data.add(kegiatan);
            }

            return data;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        } 
    }

    

    

    

    

    

    
    
    
    
    
    
}
