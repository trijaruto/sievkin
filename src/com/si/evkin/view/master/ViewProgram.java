/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Organisasi;
import com.si.evkin.model.master.Program;
import com.si.evkin.model.util.AppYears;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author TRI
 */
public class ViewProgram extends JInternalFrame implements ActionListener, ListSelectionListener, ItemListener{

    private Connection conn;
    
    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    
    private JPanel topTopPanel = new JPanel();
    
    private JLabel yearsLbl = new JLabel("Tahun");
    private JLabel yearsTitikDuaLbl = new JLabel(":");
    private JComboBox yearsCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxYears = new DefaultComboBoxModel();   
    private String selectionYears;
    
    private JLabel organisasiLbl = new JLabel("Organisasi");
    private JLabel organisasiTitikDuaLbl = new JLabel(":");
    private JComboBox organisasiCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxOrganisasi = new DefaultComboBoxModel();
    private Organisasi selectionOrganisasi = new Organisasi();
    private List<Organisasi> listOrganisasi = new ArrayList<>();
    
    private JLabel codeLbl = new JLabel("Code");
    private JLabel codeTitikDuaLbl = new JLabel(":");
    private JTextField codeTfl = new JTextField();
    
    private JLabel programLbl = new JLabel("Program");
    private JLabel programTitikDuaLbl = new JLabel(":");
    private JTextField programTfl = new JTextField();
    
    
    private JPanel topBottomPanel = new JPanel();
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");   
    
    private JScrollPane scrolPane = new JScrollPane();
    private JTable programTable = new JTable();
    private DefaultTableModel modelTableProgram = new DefaultTableModel();
    private List<Program> listProgram = new ArrayList<>();   
    private int indexrow;   
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Program selectionProgram = new Program();
    private Boolean onNeworEdit;
    private long autoindex;
    
    
    
    public ViewProgram(Connection conn){
        this.setTitle("Program");       
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(600, 400);
        this.setResizable(true);
        this.setVisible(true);
        this.conn = conn;            
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    public void component(){
        
        topPanel.setLayout(new BorderLayout()); 
        topPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"Input Program")); 
        
        topTopPanel.setLayout(new GridBagLayout());    
        topTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        topTopPanel.add(yearsLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        topTopPanel.add(yearsTitikDuaLbl, gbc);
        
        yearsCbx.setModel(modelComboboxYears);
        yearsCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        topTopPanel.add(yearsCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        topTopPanel.add(organisasiLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        topTopPanel.add(organisasiTitikDuaLbl, gbc);
        
        organisasiCbx.setModel(modelComboboxOrganisasi);
        organisasiCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        topTopPanel.add(organisasiCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 2;
        topTopPanel.add(codeLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 2;
        topTopPanel.add(codeTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 2;
        topTopPanel.add(codeTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 3;
        topTopPanel.add(programLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 3;
        topTopPanel.add(programTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 3;
        topTopPanel.add(programTfl, gbc);
                        
        topBottomPanel.setLayout(new FlowLayout());
        topBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        topBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        topBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        topBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        topBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        topBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
                               
        topPanel.add(topTopPanel, BorderLayout.NORTH);
        topPanel.add(topBottomPanel, BorderLayout.SOUTH);
        
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Program")); 
        
        modelTableProgram = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column)
            {
              return false;
            }
        };                      
        programTable = new JTable(modelTableProgram);   
        programTable.getSelectionModel().addListSelectionListener(this);       
        init();              
        programTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        programTable.setRowSelectionAllowed(true);
        programTable.getColumnModel().getColumn(0).setMinWidth(0);
        programTable.getColumnModel().getColumn(0).setMaxWidth(0);
        
        scrolPane = new JScrollPane(programTable);
        bottomPanel.add(scrolPane);
                                                        
        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);                                        
        
        this.add(spLeft);        
        
    }
    
    public void init(){
        try {
            
            modelTableProgram.addColumn("autoindex");
            modelTableProgram.addColumn("Tahun");
            modelTableProgram.addColumn("Organisasi");
            modelTableProgram.addColumn("Code");
            modelTableProgram.addColumn("Name");            
            
            Organisasi organisasi = new Organisasi();
            organisasi.setAutoindex(0);
            organisasi.setCode("");
            organisasi.setName("");
            
            modelComboboxYears.addElement("");
            for(int i=0; i<AppYears.initYears().size();i++){
               modelComboboxYears.addElement(AppYears.initYears().get(i)); 
            }
            
            listOrganisasi = ctrlEvkin.onLoadOrganisasi(conn); 
            modelComboboxOrganisasi.addElement(organisasi);
            for(Organisasi urs : listOrganisasi){
                modelComboboxOrganisasi.addElement(urs);
            }
                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
    public void setDisableComponentOnStart(){
        codeTfl.setText("");
        programTfl.setText("");
        
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        programTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    public void setDisableComponentOnSelection(){       
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        programTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        codeTfl.setText("");
        programTfl.setText("");
        
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        programTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        programTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        programTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            codeTfl.setText(selectionProgram.getCode());
            programTfl.setText(selectionProgram.getName());
           
            organisasiCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            programTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            codeTfl.setText("");
            programTfl.setText("");

            organisasiCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            programTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        codeTfl.setText("");
        programTfl.setText("");
        
        organisasiCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        programTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {        
        if (!e.getValueIsAdjusting()) {           
            try {
                indexrow = programTable.getSelectedRow();
                autoindex= (long) programTable.getModel().getValueAt(indexrow, 0);
                System.out.println("autoindex : "+autoindex);
                  
                selectionProgram = ctrlEvkin.getProgramByAutoindex(autoindex, conn);
              
                if(selectionProgram!=null){
                    
                    System.out.println("autoindex : "+selectionProgram.getAutoindex());
                    System.out.println("code : "+selectionProgram.getCode());
                    System.out.println("name : "+selectionProgram.getName());
                                    
                    codeTfl.setText(selectionProgram.getCode());
                    programTfl.setText(selectionProgram.getName());  
                    
                    setDisableComponentOnSelection();
                 }
                
            } catch (Exception ex) {
                Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
    }
    
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource()==organisasiCbx){
            if (e.getStateChange() == ItemEvent.SELECTED) {
                try {
                    Object item = e.getItem();
                    System.out.println("itemStateChanged");
                    selectionOrganisasi = (Organisasi)item;
                    System.out.println("selectionOrganisasi : "+selectionOrganisasi.getAutoindex());

                    this.removeTable();
                    listProgram = ctrlEvkin.onLoadProgramByIndexOrganisasiAndYear(selectionOrganisasi.getAutoindex(), selectionYears, conn);
                    if(!listProgram.isEmpty()){
                        for(Program pmrth : listProgram){
                            modelTableProgram.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionOrganisasi.getName(), pmrth.getCode(), pmrth.getName()});
                        }  
                    }                                     
                }catch (Exception ex) {
                    Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
                }          
            }
        }else if(e.getSource()==yearsCbx){
             if (e.getStateChange() == ItemEvent.SELECTED) {
                try {
                    Object item = e.getItem();
                    System.out.println("itemStateChanged");
                    selectionYears = (String)item;
                    System.out.println("selectionYears : "+selectionYears);

                    this.removeTable();
                    listProgram = ctrlEvkin.onLoadProgramByIndexOrganisasiAndYear(selectionOrganisasi.getAutoindex(), selectionYears, conn);
                    if(!listProgram.isEmpty()){
                        for(Program pmrth : listProgram){
                            modelTableProgram.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionOrganisasi.getName(), pmrth.getCode(), pmrth.getName()});
                        }  
                    }                                     
                }catch (Exception ex) {
                    Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
                }          
            }
        }
        
    }
    
    public void removeTable(){
        listProgram.clear();
        DefaultTableModel dtm = (DefaultTableModel) programTable.getModel();
        int rowCount = dtm.getRowCount();                
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
    }
         
                    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){ 
        if(yearsCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "TAHUN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(organisasiCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "ORGANISASI Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(codeTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "KODE Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(programTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "PROGRAM Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }          
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Program program = new Program();
            program.setIndexOrganisasi(selectionOrganisasi.getAutoindex());
            program.setYears(selectionYears);
            program.setCode(codeTfl.getText());
            program.setName(programTfl.getText());                               
            autoindex = ctrlEvkin.onSaveMasterProgram(program, conn);  
            program.setAutoindex(autoindex);
            selectionProgram = program;
            
            this.removeTable();
            listProgram = ctrlEvkin.onLoadProgramByIndexOrganisasiAndYear(selectionOrganisasi.getAutoindex(), selectionYears, conn);
            for(Program pmrth : listProgram){
                modelTableProgram.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionOrganisasi.getName(), pmrth.getCode(), pmrth.getName()});
            }            
            
        } catch (Exception ex) {
            Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {           
            Program program = new Program();
            program.setAutoindex(selectionProgram.getAutoindex());
            program.setIndexOrganisasi(selectionOrganisasi.getAutoindex());
            program.setYears(selectionYears);
            program.setCode(codeTfl.getText());
            program.setName(programTfl.getText());   
            selectionProgram = program;              
            ctrlEvkin.onUpdateMasterProgram(program, conn); 
            
            this.removeTable();
            listProgram = ctrlEvkin.onLoadProgramByIndexOrganisasiAndYear(selectionOrganisasi.getAutoindex(), selectionYears, conn);
            for(Program pmrth : listProgram){
                modelTableProgram.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionOrganisasi.getName(), pmrth.getCode(), pmrth.getName()});
            }                         
        } catch (Exception ex) {
            Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterProgramByAutoindex(selectionProgram.getAutoindex(), conn); 
            modelTableProgram.removeRow(indexrow);
            indexrow = 0;
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewProgram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }           
 
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel topPanel) {
        this.topPanel = topPanel;
    }

    public JPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(JPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public JPanel getTopTopPanel() {
        return topTopPanel;
    }

    public void setTopTopPanel(JPanel topTopPanel) {
        this.topTopPanel = topTopPanel;
    }

    public JLabel getCodeLbl() {
        return codeLbl;
    }

    public void setCodeLbl(JLabel codeLbl) {
        this.codeLbl = codeLbl;
    }

    public JLabel getCodeTitikDuaLbl() {
        return codeTitikDuaLbl;
    }

    public void setCodeTitikDuaLbl(JLabel codeTitikDuaLbl) {
        this.codeTitikDuaLbl = codeTitikDuaLbl;
    }

    public JTextField getCodeTfl() {
        return codeTfl;
    }

    public void setCodeTfl(JTextField codeTfl) {
        this.codeTfl = codeTfl;
    }

    public JLabel getProgramLbl() {
        return programLbl;
    }

    public void setProgramLbl(JLabel programLbl) {
        this.programLbl = programLbl;
    }

    public JLabel getProgramTitikDuaLbl() {
        return programTitikDuaLbl;
    }

    public void setProgramTitikDuaLbl(JLabel programTitikDuaLbl) {
        this.programTitikDuaLbl = programTitikDuaLbl;
    }

    public JTextField getProgramTfl() {
        return programTfl;
    }

    public void setProgramTfl(JTextField programTfl) {
        this.programTfl = programTfl;
    }

    public JPanel getTopBottomPanel() {
        return topBottomPanel;
    }

    public void setTopBottomPanel(JPanel topBottomPanel) {
        this.topBottomPanel = topBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public JScrollPane getScrolPane() {
        return scrolPane;
    }

    public void setScrolPane(JScrollPane scrolPane) {
        this.scrolPane = scrolPane;
    }

    public JTable getProgramTable() {
        return programTable;
    }

    public void setProgramTable(JTable programTable) {
        this.programTable = programTable;
    }

    public DefaultTableModel getModelTableProgram() {
        return modelTableProgram;
    }

    public void setModelTableProgram(DefaultTableModel modelTableProgram) {
        this.modelTableProgram = modelTableProgram;
    }

    public List<Program> getListProgram() {
        return listProgram;
    }

    public void setListProgram(List<Program> listProgram) {
        this.listProgram = listProgram;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }

    public Program getSelectionProgram() {
        return selectionProgram;
    }

    public void setSelectionProgram(Program selectionProgram) {
        this.selectionProgram = selectionProgram;
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    

    
    
    
    
    
    
}
