/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Pemerintahan;
import com.si.evkin.model.master.Urusan;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author TRI
 */
public class ViewPemerintahan extends JInternalFrame implements ActionListener, ListSelectionListener, ItemListener{

    private Connection conn;
    
    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    
    private JPanel topTopPanel = new JPanel();
    
    private JLabel urusanLbl = new JLabel("Urusan");
    private JLabel urusanTitikDuaLbl = new JLabel(":");
    private JComboBox urusanCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxUrusan = new DefaultComboBoxModel();
    private Urusan selectionUrusan = new Urusan();
    private List<Urusan> listUrusan = new ArrayList<>();
    
    private JLabel codeLbl = new JLabel("Code");
    private JLabel codeTitikDuaLbl = new JLabel(":");
    private JTextField codeTfl = new JTextField();
    
    private JLabel pemerintahanLbl = new JLabel("Pemerintahan");
    private JLabel pemerintahanTitikDuaLbl = new JLabel(":");
    private JTextField pemerintahanTfl = new JTextField();
    
    
    private JPanel topBottomPanel = new JPanel();
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");   
    
    private JScrollPane scrolPane = new JScrollPane();
    private JTable pemerintahanTable = new JTable();
    private DefaultTableModel modelTablePemerintahan = new DefaultTableModel();
    private List<Pemerintahan> listPemerintahan = new ArrayList<>();   
    private int indexrow;   
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Pemerintahan selectionPemerintahan = new Pemerintahan();
    private Boolean onNeworEdit;
    private long autoindex;
    
    
    
    public ViewPemerintahan(Connection conn){
        this.setTitle("Pemerintahan");
        this.setResizable(true);
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(500, 400);
        this.setResizable(false);
        this.setVisible(true);
        this.conn = conn;            
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    public void component(){
        
        topPanel.setLayout(new BorderLayout()); 
        topPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"Input Pemerintahan")); 
        
        topTopPanel.setLayout(new GridBagLayout());    
        topTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        topTopPanel.add(urusanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        topTopPanel.add(urusanTitikDuaLbl, gbc);
        
        urusanCbx.setModel(modelComboboxUrusan);
        urusanCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        topTopPanel.add(urusanCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        topTopPanel.add(codeLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        topTopPanel.add(codeTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        topTopPanel.add(codeTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 2;
        topTopPanel.add(pemerintahanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 2;
        topTopPanel.add(pemerintahanTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 2;
        topTopPanel.add(pemerintahanTfl, gbc);
                        
        topBottomPanel.setLayout(new FlowLayout());
        topBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        topBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        topBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        topBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        topBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        topBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
                               
        topPanel.add(topTopPanel, BorderLayout.NORTH);
        topPanel.add(topBottomPanel, BorderLayout.SOUTH);
        
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Pemerintahan")); 
        
        modelTablePemerintahan = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column)
            {
              return false;
            }
        };                      
        pemerintahanTable = new JTable(modelTablePemerintahan);   
        pemerintahanTable.getSelectionModel().addListSelectionListener(this);       
        init();              
        pemerintahanTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pemerintahanTable.setRowSelectionAllowed(true);
        pemerintahanTable.getColumnModel().getColumn(0).setMinWidth(0);
        pemerintahanTable.getColumnModel().getColumn(0).setMaxWidth(0);
        
        scrolPane = new JScrollPane(pemerintahanTable);
        bottomPanel.add(scrolPane);
                                                        
        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);                                        
        
        this.add(spLeft);        
        
    }
    
    public void init(){
        try {
            
            modelTablePemerintahan.addColumn("autoindex");
            modelTablePemerintahan.addColumn("Urusan");
            modelTablePemerintahan.addColumn("Code");
            modelTablePemerintahan.addColumn("Name");
                                    
//            listPemerintahan = ctrlEvkin.onLoadPemerintahan(conn); 
//            for(Pemerintahan pmrth : listPemerintahan){
//                modelTablePemerintahan.addRow(new Object[]{pmrth.getAutoindex(), pmrth.getCode(), pmrth.getName()});
//            }
            
            Urusan urusan = new Urusan();
            urusan.setAutoindex(0);
            urusan.setCode("");
            urusan.setName("");
            
            listUrusan = ctrlEvkin.onLoadUrusan(conn); 
            modelComboboxUrusan.addElement(urusan);
            for(Urusan urs : listUrusan){
                modelComboboxUrusan.addElement(urs);
            }
                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
    public void setDisableComponentOnStart(){
        codeTfl.setText("");
        pemerintahanTfl.setText("");
        
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        pemerintahanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    public void setDisableComponentOnSelection(){       
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        pemerintahanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        codeTfl.setText("");
        pemerintahanTfl.setText("");
        
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        pemerintahanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        pemerintahanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        pemerintahanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            codeTfl.setText(selectionPemerintahan.getCode());
            pemerintahanTfl.setText(selectionPemerintahan.getName());
           
            urusanCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            pemerintahanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            codeTfl.setText("");
            pemerintahanTfl.setText("");

            urusanCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            pemerintahanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        codeTfl.setText("");
        pemerintahanTfl.setText("");
        
        urusanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        pemerintahanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {        
        if (!e.getValueIsAdjusting()) {           
            try {
                indexrow = pemerintahanTable.getSelectedRow();
                autoindex= (long) pemerintahanTable.getModel().getValueAt(indexrow, 0);
                System.out.println("autoindex : "+autoindex);
                  
                selectionPemerintahan = ctrlEvkin.getPemerintahanByAutoindex(autoindex, conn);
              
                if(selectionPemerintahan!=null){
                    
                    System.out.println("autoindex : "+selectionPemerintahan.getAutoindex());
                    System.out.println("code : "+selectionPemerintahan.getCode());
                    System.out.println("name : "+selectionPemerintahan.getName());
                
                    
                    codeTfl.setText(selectionPemerintahan.getCode());
                    pemerintahanTfl.setText(selectionPemerintahan.getName());  
                    
                    setDisableComponentOnSelection();
                 }
                
            } catch (Exception ex) {
                Logger.getLogger(ViewPemerintahan.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
    }
    
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            try {
                Object item = e.getItem();
                System.out.println("itemStateChanged");
                selectionUrusan = (Urusan)item;
                System.out.println("selectionUrusan : "+selectionUrusan.getAutoindex());
                
                this.removeTable();
                listPemerintahan = ctrlEvkin.onLoadPemerintahanByIndexUrusan(selectionUrusan.getAutoindex(), conn);
                for(Pemerintahan pmrth : listPemerintahan){
                    modelTablePemerintahan.addRow(new Object[]{pmrth.getAutoindex(), selectionUrusan.getName(), pmrth.getCode(), pmrth.getName()});
                }                  
            }catch (Exception ex) {
                Logger.getLogger(ViewPemerintahan.class.getName()).log(Level.SEVERE, null, ex);
            }          
        }
    }
    
    public void removeTable(){
        listPemerintahan.clear();
        DefaultTableModel dtm = (DefaultTableModel) pemerintahanTable.getModel();
        int rowCount = dtm.getRowCount();                
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
    }
         
                    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){                      
        if(urusanCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "URUSAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(codeTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "KODE Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(pemerintahanTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "PEMERINTAHAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }          
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Pemerintahan pemerintahan = new Pemerintahan();
            pemerintahan.setIndexUrusan(selectionUrusan.getAutoindex());
            pemerintahan.setCode(codeTfl.getText());
            pemerintahan.setName(pemerintahanTfl.getText());                               
            autoindex = ctrlEvkin.onSaveMasterPemerintahan(pemerintahan, conn);  
            pemerintahan.setAutoindex(autoindex);
            selectionPemerintahan = pemerintahan;
            
            this.removeTable();
            listPemerintahan = ctrlEvkin.onLoadPemerintahanByIndexUrusan(selectionUrusan.getAutoindex(), conn);
            for(Pemerintahan pmrth : listPemerintahan){
                modelTablePemerintahan.addRow(new Object[]{pmrth.getAutoindex(), selectionUrusan.getName(), pmrth.getCode(), pmrth.getName()});
            }            
            
        } catch (Exception ex) {
            Logger.getLogger(ViewPemerintahan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {           
            Pemerintahan pemerintahan = new Pemerintahan();
            pemerintahan.setAutoindex(selectionPemerintahan.getAutoindex());
            pemerintahan.setIndexUrusan(selectionUrusan.getAutoindex());
            pemerintahan.setCode(codeTfl.getText());
            pemerintahan.setName(pemerintahanTfl.getText());   
            selectionPemerintahan = pemerintahan;              
            ctrlEvkin.onUpdateMasterPemerintahan(pemerintahan, conn); 
            
            this.removeTable();
            listPemerintahan = ctrlEvkin.onLoadPemerintahanByIndexUrusan(selectionUrusan.getAutoindex(), conn);
            for(Pemerintahan pmrth : listPemerintahan){
                modelTablePemerintahan.addRow(new Object[]{pmrth.getAutoindex(), selectionUrusan.getName(), pmrth.getCode(), pmrth.getName()});
            }                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPemerintahan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterPemerintahanByAutoindex(selectionPemerintahan.getAutoindex(), conn); 
            modelTablePemerintahan.removeRow(indexrow);
            indexrow = 0;
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewPemerintahan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }           
 
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel topPanel) {
        this.topPanel = topPanel;
    }

    public JPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(JPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public JPanel getTopTopPanel() {
        return topTopPanel;
    }

    public void setTopTopPanel(JPanel topTopPanel) {
        this.topTopPanel = topTopPanel;
    }

    public JLabel getCodeLbl() {
        return codeLbl;
    }

    public void setCodeLbl(JLabel codeLbl) {
        this.codeLbl = codeLbl;
    }

    public JLabel getCodeTitikDuaLbl() {
        return codeTitikDuaLbl;
    }

    public void setCodeTitikDuaLbl(JLabel codeTitikDuaLbl) {
        this.codeTitikDuaLbl = codeTitikDuaLbl;
    }

    public JTextField getCodeTfl() {
        return codeTfl;
    }

    public void setCodeTfl(JTextField codeTfl) {
        this.codeTfl = codeTfl;
    }

    public JLabel getPemerintahanLbl() {
        return pemerintahanLbl;
    }

    public void setPemerintahanLbl(JLabel pemerintahanLbl) {
        this.pemerintahanLbl = pemerintahanLbl;
    }

    public JLabel getPemerintahanTitikDuaLbl() {
        return pemerintahanTitikDuaLbl;
    }

    public void setPemerintahanTitikDuaLbl(JLabel pemerintahanTitikDuaLbl) {
        this.pemerintahanTitikDuaLbl = pemerintahanTitikDuaLbl;
    }

    public JTextField getPemerintahanTfl() {
        return pemerintahanTfl;
    }

    public void setPemerintahanTfl(JTextField pemerintahanTfl) {
        this.pemerintahanTfl = pemerintahanTfl;
    }

    public JPanel getTopBottomPanel() {
        return topBottomPanel;
    }

    public void setTopBottomPanel(JPanel topBottomPanel) {
        this.topBottomPanel = topBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public JScrollPane getScrolPane() {
        return scrolPane;
    }

    public void setScrolPane(JScrollPane scrolPane) {
        this.scrolPane = scrolPane;
    }

    public JTable getPemerintahanTable() {
        return pemerintahanTable;
    }

    public void setPemerintahanTable(JTable pemerintahanTable) {
        this.pemerintahanTable = pemerintahanTable;
    }

    public DefaultTableModel getModelTablePemerintahan() {
        return modelTablePemerintahan;
    }

    public void setModelTablePemerintahan(DefaultTableModel modelTablePemerintahan) {
        this.modelTablePemerintahan = modelTablePemerintahan;
    }

    public List<Pemerintahan> getListPemerintahan() {
        return listPemerintahan;
    }

    public void setListPemerintahan(List<Pemerintahan> listPemerintahan) {
        this.listPemerintahan = listPemerintahan;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }

    public Pemerintahan getSelectionPemerintahan() {
        return selectionPemerintahan;
    }

    public void setSelectionPemerintahan(Pemerintahan selectionPemerintahan) {
        this.selectionPemerintahan = selectionPemerintahan;
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    

    
    
    
    
    
    
}
