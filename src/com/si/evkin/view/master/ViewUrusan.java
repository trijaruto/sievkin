/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Urusan;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author TRI
 */
public class ViewUrusan extends JInternalFrame implements ActionListener, ListSelectionListener{

    private Connection conn;
    
    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    
    private JPanel topTopPanel = new JPanel();
    private JLabel codeLbl = new JLabel("Code");
    private JLabel codeTitikDuaLbl = new JLabel(":");
    private JTextField codeTfl = new JTextField();
    
    private JLabel urusanLbl = new JLabel("Urusan");
    private JLabel urusanTitikDuaLbl = new JLabel(":");
    private JTextField urusanTfl = new JTextField();
    
    
    private JPanel topBottomPanel = new JPanel();
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");   
    
    private JScrollPane scrolPane = new JScrollPane();
    private JTable urusanTable = new JTable();
    private DefaultTableModel modelTableUrusan = new DefaultTableModel();
    private List<Urusan> listUrusan = new ArrayList<Urusan>();   
    private int indexrow;   
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Urusan selectionUrusan = new Urusan();
    private Boolean onNeworEdit;
    private long autoindex;
    
    
    
    public ViewUrusan(Connection conn){
        this.setTitle("Urusan");
        this.setResizable(true);
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(500, 400);
        this.setResizable(false);
        this.setVisible(true);
        this.conn = conn;            
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    public void component(){
        
        topPanel.setLayout(new BorderLayout()); 
        topPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"Input Urusan")); 
        
        topTopPanel.setLayout(new GridBagLayout());    
        topTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        topTopPanel.add(codeLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        topTopPanel.add(codeTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        topTopPanel.add(codeTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        topTopPanel.add(urusanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        topTopPanel.add(urusanTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        topTopPanel.add(urusanTfl, gbc);
                        
        topBottomPanel.setLayout(new FlowLayout());
        topBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        topBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        topBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        topBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        topBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        topBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
                               
        topPanel.add(topTopPanel, BorderLayout.NORTH);
        topPanel.add(topBottomPanel, BorderLayout.SOUTH);
        
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Urusan Pemerintahan")); 
                
        String[] columnNames = {"autoindex","Kode","Urusan"};                        
        modelTableUrusan = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column)
            {
              return false;
            }
        };                      
        urusanTable = new JTable(modelTableUrusan);   
        urusanTable.getSelectionModel().addListSelectionListener(this);
       // urusanTable.getColumnModel().getSelectionModel().addListSelectionListener(this);
        init();              
        urusanTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        urusanTable.setRowSelectionAllowed(true);
        urusanTable.getColumnModel().getColumn(0).setMinWidth(0);
        urusanTable.getColumnModel().getColumn(0).setMaxWidth(0);
        
        scrolPane = new JScrollPane(urusanTable);
        bottomPanel.add(scrolPane);
                                                        
        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);                                        
        
        this.add(spLeft);        
        
    }
    
    public void init(){
        try {
            
            modelTableUrusan.addColumn("autoindex");
            modelTableUrusan.addColumn("Code");
            modelTableUrusan.addColumn("Name");
                                    
            listUrusan = ctrlEvkin.onLoadUrusan(conn); 
            for(Urusan urs : listUrusan){
                modelTableUrusan.addRow(new Object[]{urs.getAutoindex(), urs.getCode(), urs.getName()});
            }
                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
    public void setDisableComponentOnStart(){
        codeTfl.setText("");
        urusanTfl.setText("");
        
        codeTfl.setEnabled(false);
        urusanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    public void setDisableComponentOnSelection(){       
        codeTfl.setEnabled(false);
        urusanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        codeTfl.setText("");
        urusanTfl.setText("");
        
        codeTfl.setEnabled(true);
        urusanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        codeTfl.setEnabled(false);
        urusanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        
        codeTfl.setEnabled(true);
        urusanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            codeTfl.setText(selectionUrusan.getCode());
            urusanTfl.setText(selectionUrusan.getName());
           
            codeTfl.setEnabled(false);
            urusanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            codeTfl.setText("");
            urusanTfl.setText("");

            codeTfl.setEnabled(false);
            urusanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        codeTfl.setText("");
        urusanTfl.setText("");
        
        codeTfl.setEnabled(false);
        urusanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {        
        if (!e.getValueIsAdjusting()) {           
            try {
                indexrow = urusanTable.getSelectedRow();
                autoindex= (long) urusanTable.getModel().getValueAt(indexrow, 0);
                System.out.println("autoindex : "+autoindex);
                  
                selectionUrusan = ctrlEvkin.getUrusanByAutoindex(autoindex, conn);
              
                if(selectionUrusan!=null){
                    
                    System.out.println("autoindex : "+selectionUrusan.getAutoindex());
                    System.out.println("code : "+selectionUrusan.getCode());
                    System.out.println("name : "+selectionUrusan.getName());
                
                    
                    codeTfl.setText(selectionUrusan.getCode());
                    urusanTfl.setText(selectionUrusan.getName());  
                    
                    setDisableComponentOnSelection();
                 }
                
            } catch (Exception ex) {
                Logger.getLogger(ViewUrusan.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
    }
         
                    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){                      
        if(codeTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "KODE Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(urusanTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "URUSAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }          
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Urusan urusan = new Urusan();
            urusan.setCode(codeTfl.getText());
            urusan.setName(urusanTfl.getText());                               
            autoindex = ctrlEvkin.onSaveMasterUrusan(urusan, conn);  
            urusan.setAutoindex(autoindex);
            selectionUrusan = urusan;
            
            DefaultTableModel model = (DefaultTableModel) urusanTable.getModel();
            model.addRow(new Object[]{urusan.getAutoindex(),urusan.getCode(), urusan.getName()});
            
        } catch (Exception ex) {
            Logger.getLogger(ViewUrusan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {           
            Urusan urusan = new Urusan();
            urusan.setAutoindex(selectionUrusan.getAutoindex());
            urusan.setCode(codeTfl.getText());
            urusan.setName(urusanTfl.getText());   
            selectionUrusan = urusan;              
            ctrlEvkin.onUpdateMasterUrusan(urusan, conn);  
            for(int i=0; i<listUrusan.size(); i++){
                if(i == indexrow){
                    modelTableUrusan.setValueAt(selectionUrusan.getAutoindex(), i, 0);
                    modelTableUrusan.setValueAt(codeTfl.getText(), i, 1);
                    modelTableUrusan.setValueAt(urusanTfl.getText(), i, 2);
                    
                    listUrusan.set(i, urusan);
                }
            }                        
        } catch (Exception ex) {
            Logger.getLogger(ViewUrusan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterUrusanByAutoindex(selectionUrusan.getAutoindex(), conn); 
            modelTableUrusan.removeRow(indexrow);
            indexrow = 0;
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewUrusan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
    
    
    
    
    

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel topPanel) {
        this.topPanel = topPanel;
    }

    public JPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(JPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public JPanel getTopTopPanel() {
        return topTopPanel;
    }

    public void setTopTopPanel(JPanel topTopPanel) {
        this.topTopPanel = topTopPanel;
    }

    public JLabel getCodeLbl() {
        return codeLbl;
    }

    public void setCodeLbl(JLabel codeLbl) {
        this.codeLbl = codeLbl;
    }

    public JLabel getCodeTitikDuaLbl() {
        return codeTitikDuaLbl;
    }

    public void setCodeTitikDuaLbl(JLabel codeTitikDuaLbl) {
        this.codeTitikDuaLbl = codeTitikDuaLbl;
    }

    public JTextField getCodeTfl() {
        return codeTfl;
    }

    public void setCodeTfl(JTextField codeTfl) {
        this.codeTfl = codeTfl;
    }

    public JLabel getUrusanLbl() {
        return urusanLbl;
    }

    public void setUrusanLbl(JLabel urusanLbl) {
        this.urusanLbl = urusanLbl;
    }

    public JLabel getUrusanTitikDuaLbl() {
        return urusanTitikDuaLbl;
    }

    public void setUrusanTitikDuaLbl(JLabel urusanTitikDuaLbl) {
        this.urusanTitikDuaLbl = urusanTitikDuaLbl;
    }

    public JTextField getUrusanTfl() {
        return urusanTfl;
    }

    public void setUrusanTfl(JTextField urusanTfl) {
        this.urusanTfl = urusanTfl;
    }

    public JPanel getTopBottomPanel() {
        return topBottomPanel;
    }

    public void setTopBottomPanel(JPanel topBottomPanel) {
        this.topBottomPanel = topBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public JScrollPane getScrolPane() {
        return scrolPane;
    }

    public void setScrolPane(JScrollPane scrolPane) {
        this.scrolPane = scrolPane;
    }

    public JTable getUrusanTable() {
        return urusanTable;
    }

    public void setUrusanTable(JTable urusanTable) {
        this.urusanTable = urusanTable;
    }

    public DefaultTableModel getModelTableUrusan() {
        return modelTableUrusan;
    }

    public void setModelTableUrusan(DefaultTableModel modelTableUrusan) {
        this.modelTableUrusan = modelTableUrusan;
    }

    public List<Urusan> getListUrusan() {
        return listUrusan;
    }

    public void setListUrusan(List<Urusan> listUrusan) {
        this.listUrusan = listUrusan;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }

    public Urusan getSelectionUrusan() {
        return selectionUrusan;
    }

    public void setSelectionUrusan(Urusan selectionUrusan) {
        this.selectionUrusan = selectionUrusan;
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    
    
    
    
    
    
}
