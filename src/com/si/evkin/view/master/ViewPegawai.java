/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Pegawai;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author TRI
 */
public class ViewPegawai extends JInternalFrame implements ActionListener, ListSelectionListener {

    private Connection conn;
    
    private JPanel leftPanel = new JPanel();
    private JPanel rightPanel = new JPanel();
    
    private JPanel rightTopPanel = new JPanel();
    
    private JLabel nipLbl = new JLabel("NIP");
    private JLabel nipTitikDuaLbl = new JLabel(":");
    private JTextField nipTfl = new JTextField();
    
    private JLabel nameLbl = new JLabel("Nama");
    private JLabel nameTitikDuaLbl = new JLabel(":");
    private JTextField nameTfl = new JTextField();
    
    private JLabel jabatanLbl = new JLabel("Jabatan");
    private JLabel jabatanTitikDuaLbl = new JLabel(":");
    private JTextField jabatanTfl = new JTextField();
    
    private JLabel golonganLbl = new JLabel("Golongan");
    private JLabel golonganTitikDuaLbl = new JLabel(":");
    private JTextField golonganTfl = new JTextField();
    
    private JLabel eselonLbl = new JLabel("Eselon");
    private JLabel eselonTitikDuaLbl = new JLabel(":");
    private JTextField eselonTfl = new JTextField();
    
        
    private JPanel rightBottomPanel = new JPanel();    
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");           
            
    private DefaultListModel modelPegawai = new DefaultListModel();    
    private JList list = new JList();    
    private long indexList;
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Pegawai selectionPegawai = new Pegawai();
    private Boolean onNeworEdit;
    private long autoindex;
    
    public ViewPegawai(Connection conn){
        this.setTitle("Pegawai");
        this.setResizable(true);
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(800, 400);
        this.setResizable(false);
        this.setVisible(true);
        this.conn = conn;     
        this.init();
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    
    public void component(){                               
        
        list.setModel(modelPegawai);
        list.setFixedCellWidth(250);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);    
        list.addListSelectionListener(this); 
        
        leftPanel.setLayout(new GridLayout()); 
        leftPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"NIP / Nama"));        
        leftPanel.add(new JScrollPane(list));   
                       
        rightPanel.setLayout(new BorderLayout());
        rightPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Input Pegawai")); 
        
        rightTopPanel.setLayout(new GridBagLayout());    
        rightTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        rightTopPanel.add(nipLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        rightTopPanel.add(nipTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        rightTopPanel.add(nipTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        rightTopPanel.add(nameLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        rightTopPanel.add(nameTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        rightTopPanel.add(nameTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 2;
        rightTopPanel.add(jabatanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 2;
        rightTopPanel.add(jabatanTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 2;
        rightTopPanel.add(jabatanTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 3;
        rightTopPanel.add(golonganLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 3;
        rightTopPanel.add(golonganTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 3;
        rightTopPanel.add(golonganTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 4;
        rightTopPanel.add(eselonLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 4;
        rightTopPanel.add(eselonTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 4;
        rightTopPanel.add(eselonTfl, gbc);
        
        
        rightBottomPanel.setLayout(new FlowLayout());
        rightBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        rightBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        rightBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        rightBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        rightBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        rightBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
        
        rightPanel.add(rightTopPanel, BorderLayout.NORTH);
        rightPanel.add(rightBottomPanel, BorderLayout.SOUTH);
                
        JSplitPane spLeft = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);                                        
        
        this.add(spLeft);        
    }
    
    public void init(){
        try {
            
            modelPegawai = ctrlEvkin.onLoadPegawai(conn); 
                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDisableComponentOnStart(){
        nipTfl.setText("");
        nameTfl.setText("");
        golonganTfl.setText("");
        jabatanTfl.setText("");
        eselonTfl.setText("");
        
        nipTfl.setEnabled(false);
        nameTfl.setEnabled(false);
        golonganTfl.setEnabled(false);
        jabatanTfl.setEnabled(false);
        eselonTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    public void setDisableComponentOnSelection(){       
        nipTfl.setEnabled(false);
        nameTfl.setEnabled(false);
        golonganTfl.setEnabled(false);
        jabatanTfl.setEnabled(false);
        eselonTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        nipTfl.setText("");
        nameTfl.setText("");
        golonganTfl.setText("");
        jabatanTfl.setText("");
        eselonTfl.setText("");
        
        nipTfl.setEnabled(true);
        nameTfl.setEnabled(true);
        golonganTfl.setEnabled(true);
        jabatanTfl.setEnabled(true);
        eselonTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        nipTfl.setEnabled(false);
        nameTfl.setEnabled(false);
        golonganTfl.setEnabled(false);
        jabatanTfl.setEnabled(false);
        eselonTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        
        nipTfl.setEnabled(true);
        nameTfl.setEnabled(true);
        golonganTfl.setEnabled(true);
        jabatanTfl.setEnabled(true);
        eselonTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            nipTfl.setText(selectionPegawai.getNip());
            nameTfl.setText(selectionPegawai.getName());
            golonganTfl.setText(selectionPegawai.getGolongan());
            jabatanTfl.setText(selectionPegawai.getJabatan());
            eselonTfl.setText(selectionPegawai.getEselon());

            nipTfl.setEnabled(false);
            nameTfl.setEnabled(false);
            golonganTfl.setEnabled(false);
            jabatanTfl.setEnabled(false);
            eselonTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            nipTfl.setText("");
            nameTfl.setText("");
            golonganTfl.setText("");
            jabatanTfl.setText("");
            eselonTfl.setText("");

            nipTfl.setEnabled(false);
            nameTfl.setEnabled(false);
            golonganTfl.setEnabled(false);
            jabatanTfl.setEnabled(false);
            eselonTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        nipTfl.setText("");
        nameTfl.setText("");
        golonganTfl.setText("");
        jabatanTfl.setText("");
        eselonTfl.setText("");
        
        nipTfl.setEnabled(false);
        nameTfl.setEnabled(false);
        golonganTfl.setEnabled(false);
        jabatanTfl.setEnabled(false);
        eselonTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
     @Override
    public void valueChanged(ListSelectionEvent e) {       
         DefaultListModel model = (DefaultListModel) list.getModel();
         indexList = list.getSelectedIndex();
         selectionPegawai = (Pegawai) list.getSelectedValue();
        
         if(selectionPegawai!=null){
            nipTfl.setText(selectionPegawai.getNip());
            nameTfl.setText(selectionPegawai.getName());
            golonganTfl.setText(selectionPegawai.getGolongan());
            jabatanTfl.setText(selectionPegawai.getJabatan());
            eselonTfl.setText(selectionPegawai.getEselon());
            setDisableComponentOnSelection();
         }
         
    }
    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){                      
        if(nipTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "NIP Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(nameTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "NAMA Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(golonganTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "Golongan Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(jabatanTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "JABATAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(eselonTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "ESELON Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }               
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }
    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Pegawai pegawai = new Pegawai();
            pegawai.setNip(nipTfl.getText());
            pegawai.setName(nameTfl.getText());
            pegawai.setJabatan(golonganTfl.getText());
            pegawai.setGolongan(jabatanTfl.getText());
            pegawai.setEselon(eselonTfl.getText());                          
            autoindex = ctrlEvkin.onSaveMasterPegawai(pegawai, conn);  
            pegawai.setAutoindex(autoindex);
            selectionPegawai = pegawai;
            modelPegawai.addElement(pegawai);
            list.setSelectedValue(pegawai, true);
            
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {
            Pegawai pegawai = new Pegawai();             
            pegawai.setAutoindex(selectionPegawai.getAutoindex());
            pegawai.setNip(nipTfl.getText());
            pegawai.setName(nameTfl.getText());
            pegawai.setJabatan(golonganTfl.getText());
            pegawai.setGolongan(jabatanTfl.getText());
            pegawai.setEselon(eselonTfl.getText());
            selectionPegawai = pegawai;              
            ctrlEvkin.onUpdateMasterPegawai(pegawai, conn);  
            for(int i=0; i<modelPegawai.size(); i++){
                if(i == indexList){
                    modelPegawai.set(i, pegawai);
                }
            }            
            list.setSelectedValue(pegawai, true);
            
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterPegawaiByAutoindex(selectionPegawai.getAutoindex(), conn); 
            modelPegawai.removeElement(selectionPegawai);
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }   

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JList getList() {
        return list;
    }

    public void setList(JList list) {
        this.list = list;
    }

    public Pegawai getSelectionPegawai() {
        return selectionPegawai;
    }

    public void setSelectionPegawai(Pegawai selectionPegawai) {
        this.selectionPegawai = selectionPegawai;
    }   

    public long getIndexList() {
        return indexList;
    }

    public void setIndexList(long indexList) {
        this.indexList = indexList;
    }

    public DefaultListModel getModelPegawai() {
        return modelPegawai;
    }

    public void setModelPegawai(DefaultListModel modelPegawai) {
        this.modelPegawai = modelPegawai;
    }

    public JPanel getLeftPanel() {
        return leftPanel;
    }

    public void setLeftPanel(JPanel leftPanel) {
        this.leftPanel = leftPanel;
    }

    public JPanel getRightPanel() {
        return rightPanel;
    }

    public void setRightPanel(JPanel rightPanel) {
        this.rightPanel = rightPanel;
    }

    public JPanel getRightTopPanel() {
        return rightTopPanel;
    }

    public void setRightTopPanel(JPanel rightTopPanel) {
        this.rightTopPanel = rightTopPanel;
    }

    public JLabel getNipLbl() {
        return nipLbl;
    }

    public void setNipLbl(JLabel nipLbl) {
        this.nipLbl = nipLbl;
    }

    public JLabel getNipTitikDuaLbl() {
        return nipTitikDuaLbl;
    }

    public void setNipTitikDuaLbl(JLabel nipTitikDuaLbl) {
        this.nipTitikDuaLbl = nipTitikDuaLbl;
    }

    public JTextField getNipTfl() {
        return nipTfl;
    }

    public void setNipTfl(JTextField nipTfl) {
        this.nipTfl = nipTfl;
    }

    public JLabel getNameLbl() {
        return nameLbl;
    }

    public void setNameLbl(JLabel nameLbl) {
        this.nameLbl = nameLbl;
    }

    public JLabel getNameTitikDuaLbl() {
        return nameTitikDuaLbl;
    }

    public void setNameTitikDuaLbl(JLabel nameTitikDuaLbl) {
        this.nameTitikDuaLbl = nameTitikDuaLbl;
    }

    public JTextField getNameTfl() {
        return nameTfl;
    }

    public void setNameTfl(JTextField nameTfl) {
        this.nameTfl = nameTfl;
    }

    public JLabel getJabatanLbl() {
        return jabatanLbl;
    }

    public void setJabatanLbl(JLabel jabatanLbl) {
        this.jabatanLbl = jabatanLbl;
    }

    public JLabel getJabatanTitikDuaLbl() {
        return jabatanTitikDuaLbl;
    }

    public void setJabatanTitikDuaLbl(JLabel jabatanTitikDuaLbl) {
        this.jabatanTitikDuaLbl = jabatanTitikDuaLbl;
    }

    public JTextField getJabatanTfl() {
        return jabatanTfl;
    }

    public void setJabatanTfl(JTextField jabatanTfl) {
        this.jabatanTfl = jabatanTfl;
    }

    public JLabel getGolonganLbl() {
        return golonganLbl;
    }

    public void setGolonganLbl(JLabel golonganLbl) {
        this.golonganLbl = golonganLbl;
    }

    public JLabel getGolonganTitikDuaLbl() {
        return golonganTitikDuaLbl;
    }

    public void setGolonganTitikDuaLbl(JLabel golonganTitikDuaLbl) {
        this.golonganTitikDuaLbl = golonganTitikDuaLbl;
    }

    public JTextField getGolonganTfl() {
        return golonganTfl;
    }

    public void setGolonganTfl(JTextField golonganTfl) {
        this.golonganTfl = golonganTfl;
    }

    public JLabel getEselonLbl() {
        return eselonLbl;
    }

    public void setEselonLbl(JLabel eselonLbl) {
        this.eselonLbl = eselonLbl;
    }

    public JLabel getEselonTitikDuaLbl() {
        return eselonTitikDuaLbl;
    }

    public void setEselonTitikDuaLbl(JLabel eselonTitikDuaLbl) {
        this.eselonTitikDuaLbl = eselonTitikDuaLbl;
    }

    public JTextField getEselonTfl() {
        return eselonTfl;
    }

    public void setEselonTfl(JTextField eselonTfl) {
        this.eselonTfl = eselonTfl;
    }

    public JPanel getRightBottomPanel() {
        return rightBottomPanel;
    }

    public void setRightBottomPanel(JPanel rightBottomPanel) {
        this.rightBottomPanel = rightBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }
        
    
    
    
}



