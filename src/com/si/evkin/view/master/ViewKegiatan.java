/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Program;
import com.si.evkin.model.master.Kegiatan;
import com.si.evkin.model.util.AppYears;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author TRI
 */
public class ViewKegiatan extends JInternalFrame implements ActionListener, ListSelectionListener, ItemListener{

    private Connection conn;
    
    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    
    private JPanel topTopPanel = new JPanel();
    
    private JLabel yearsLbl = new JLabel("Tahun");
    private JLabel yearsTitikDuaLbl = new JLabel(":");
    private JComboBox yearsCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxYears = new DefaultComboBoxModel();   
    private String selectionYears;
    
    private JLabel programLbl = new JLabel("Program");
    private JLabel programTitikDuaLbl = new JLabel(":");
    private JComboBox programCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxProgram = new DefaultComboBoxModel();
    private Program selectionProgram = new Program();
    private List<Program> listProgram = new ArrayList<>();
    
    private JLabel codeLbl = new JLabel("Code");
    private JLabel codeTitikDuaLbl = new JLabel(":");
    private JTextField codeTfl = new JTextField();
    
    private JLabel kegiatanLbl = new JLabel("Kegiatan");
    private JLabel kegiatanTitikDuaLbl = new JLabel(":");
    private JTextField kegiatanTfl = new JTextField();
    
    
    private JPanel topBottomPanel = new JPanel();
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");   
    
    private JScrollPane scrolPane = new JScrollPane();
    private JTable kegiatanTable = new JTable();
    private DefaultTableModel modelTableKegiatan = new DefaultTableModel();
    private List<Kegiatan> listKegiatan = new ArrayList<>();   
    private int indexrow;   
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Kegiatan selectionKegiatan = new Kegiatan();
    private Boolean onNeworEdit;
    private long autoindex;
    
    
    
    public ViewKegiatan(Connection conn){
        this.setTitle("Kegiatan");       
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(600, 400);
        this.setResizable(true);
        this.setVisible(true);
        this.conn = conn;            
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    public void component(){
        
        topPanel.setLayout(new BorderLayout()); 
        topPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"Input Kegiatan")); 
        
        topTopPanel.setLayout(new GridBagLayout());    
        topTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        topTopPanel.add(yearsLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        topTopPanel.add(yearsTitikDuaLbl, gbc);
        
        yearsCbx.setModel(modelComboboxYears);
        yearsCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        topTopPanel.add(yearsCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        topTopPanel.add(programLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        topTopPanel.add(programTitikDuaLbl, gbc);
        
        programCbx.setModel(modelComboboxProgram);
        programCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        topTopPanel.add(programCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 2;
        topTopPanel.add(codeLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 2;
        topTopPanel.add(codeTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 2;
        topTopPanel.add(codeTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 3;
        topTopPanel.add(kegiatanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 3;
        topTopPanel.add(kegiatanTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 3;
        topTopPanel.add(kegiatanTfl, gbc);
                        
        topBottomPanel.setLayout(new FlowLayout());
        topBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        topBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        topBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        topBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        topBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        topBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
                               
        topPanel.add(topTopPanel, BorderLayout.NORTH);
        topPanel.add(topBottomPanel, BorderLayout.SOUTH);
        
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Kegiatan")); 
        
        modelTableKegiatan = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column)
            {
              return false;
            }
        };                      
        kegiatanTable = new JTable(modelTableKegiatan);   
        kegiatanTable.getSelectionModel().addListSelectionListener(this);       
        init();              
        kegiatanTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        kegiatanTable.setRowSelectionAllowed(true);
        kegiatanTable.getColumnModel().getColumn(0).setMinWidth(0);
        kegiatanTable.getColumnModel().getColumn(0).setMaxWidth(0);
        
        scrolPane = new JScrollPane(kegiatanTable);
        bottomPanel.add(scrolPane);
                                                        
        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);                                        
        
        this.add(spLeft);        
        
    }
    
    public void init(){
        try {
            
            modelTableKegiatan.addColumn("autoindex");
            modelTableKegiatan.addColumn("Tahun");
            modelTableKegiatan.addColumn("Program");
            modelTableKegiatan.addColumn("Code");
            modelTableKegiatan.addColumn("Name");            
            
            Program program = new Program();
            program.setAutoindex(0);
            program.setCode("");
            program.setName("");
            
            modelComboboxYears.addElement("");
            for(int i=0; i<AppYears.initYears().size();i++){
               modelComboboxYears.addElement(AppYears.initYears().get(i)); 
            }
             
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
    public void setDisableComponentOnStart(){
        codeTfl.setText("");
        kegiatanTfl.setText("");
        
        programCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        kegiatanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    public void setDisableComponentOnSelection(){       
        programCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        kegiatanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        codeTfl.setText("");
        kegiatanTfl.setText("");
        
        programCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        kegiatanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        programCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        kegiatanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        programCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        kegiatanTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            codeTfl.setText(selectionKegiatan.getCode());
            kegiatanTfl.setText(selectionKegiatan.getName());
           
            programCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            kegiatanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            codeTfl.setText("");
            kegiatanTfl.setText("");

            programCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            kegiatanTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        codeTfl.setText("");
        kegiatanTfl.setText("");
        
        programCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        kegiatanTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {        
        if (!e.getValueIsAdjusting()) {           
            try {
                indexrow = kegiatanTable.getSelectedRow();
                autoindex= (long) kegiatanTable.getModel().getValueAt(indexrow, 0);
                System.out.println("autoindex : "+autoindex);
                  
                selectionKegiatan = ctrlEvkin.getKegiatanByAutoindex(autoindex, conn);
              
                if(selectionKegiatan!=null){
                    
                    System.out.println("autoindex : "+selectionKegiatan.getAutoindex());
                    System.out.println("code : "+selectionKegiatan.getCode());
                    System.out.println("name : "+selectionKegiatan.getName());
                                    
                    codeTfl.setText(selectionKegiatan.getCode());
                    kegiatanTfl.setText(selectionKegiatan.getName());  
                    
                    setDisableComponentOnSelection();
                 }
                
            } catch (Exception ex) {
                Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
    }
    
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        
        if(e.getSource()==yearsCbx){
             if (e.getStateChange() == ItemEvent.SELECTED) {
                try {
                    Object item = e.getItem();
                    System.out.println("itemStateChanged");
                    selectionYears = (String)item;
                    System.out.println("selectionYears : "+selectionYears);

                    this.removeTable();
                    Program program = new Program();
                    program.setAutoindex(0);
                    program.setCode("");
                    program.setName("");
                    
                    listProgram.clear();
                    modelComboboxProgram.removeAllElements();
                    
                    listProgram = ctrlEvkin.onLoadProgramByYears(selectionYears, conn); 
                    modelComboboxProgram.addElement(program);
                    for(Program urs : listProgram){
                        modelComboboxProgram.addElement(urs);
                    }                            
                }catch (Exception ex) {
                    Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
                }          
            }
        }
        else if(e.getSource()==programCbx){
            if (e.getStateChange() == ItemEvent.SELECTED) {
                try {
                    Object item = e.getItem();
                    System.out.println("itemStateChanged");
                    selectionProgram = (Program)item;
                    System.out.println("selectionProgram : "+selectionProgram.getAutoindex());

                    this.removeTable();
                    listKegiatan = ctrlEvkin.onLoadKegiatanByIndexProgramAndYear(selectionProgram.getAutoindex(), selectionYears, conn);
                    if(!listKegiatan.isEmpty()){
                        for(Kegiatan pmrth : listKegiatan){
                            modelTableKegiatan.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionProgram.getName(), pmrth.getCode(), pmrth.getName()});
                        }  
                    }                                     
                }catch (Exception ex) {
                    Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
                }          
            }
        }
        
    }
    
    public void removeTable(){
        listKegiatan.clear();
        DefaultTableModel dtm = (DefaultTableModel) kegiatanTable.getModel();
        int rowCount = dtm.getRowCount();                
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
    }
         
                    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){ 
        if(yearsCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "TAHUN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(programCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "PROGRAM Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(codeTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "KODE Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(kegiatanTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "PROGRAM Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }          
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Kegiatan kegiatan = new Kegiatan();
            kegiatan.setIndexProgram(selectionProgram.getAutoindex());            
            kegiatan.setCode(codeTfl.getText());
            kegiatan.setName(kegiatanTfl.getText());                               
            autoindex = ctrlEvkin.onSaveMasterKegiatan(kegiatan, conn);  
            kegiatan.setAutoindex(autoindex);
            selectionKegiatan = kegiatan;
            
            this.removeTable();
            listKegiatan = ctrlEvkin.onLoadKegiatanByIndexProgramAndYear(selectionProgram.getAutoindex(), selectionYears, conn);
            for(Kegiatan pmrth : listKegiatan){
                modelTableKegiatan.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionProgram.getName(), pmrth.getCode(), pmrth.getName()});
            }            
            
        } catch (Exception ex) {
            Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {           
            Kegiatan kegiatan = new Kegiatan();
            kegiatan.setAutoindex(selectionKegiatan.getAutoindex());
            kegiatan.setIndexProgram(selectionProgram.getAutoindex());           
            kegiatan.setCode(codeTfl.getText());
            kegiatan.setName(kegiatanTfl.getText());   
            selectionKegiatan = kegiatan;              
            ctrlEvkin.onUpdateMasterKegiatan(kegiatan, conn); 
            
            this.removeTable();
            listKegiatan = ctrlEvkin.onLoadKegiatanByIndexProgramAndYear(selectionProgram.getAutoindex(), selectionYears, conn);
            for(Kegiatan pmrth : listKegiatan){
                modelTableKegiatan.addRow(new Object[]{pmrth.getAutoindex(), selectionYears, selectionProgram.getName(), pmrth.getCode(), pmrth.getName()});
            }                         
        } catch (Exception ex) {
            Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterKegiatanByAutoindex(selectionKegiatan.getAutoindex(), conn); 
            modelTableKegiatan.removeRow(indexrow);
            indexrow = 0;
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewKegiatan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }           
 
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel topPanel) {
        this.topPanel = topPanel;
    }

    public JPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(JPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public JPanel getTopTopPanel() {
        return topTopPanel;
    }

    public void setTopTopPanel(JPanel topTopPanel) {
        this.topTopPanel = topTopPanel;
    }

    public JLabel getCodeLbl() {
        return codeLbl;
    }

    public void setCodeLbl(JLabel codeLbl) {
        this.codeLbl = codeLbl;
    }

    public JLabel getCodeTitikDuaLbl() {
        return codeTitikDuaLbl;
    }

    public void setCodeTitikDuaLbl(JLabel codeTitikDuaLbl) {
        this.codeTitikDuaLbl = codeTitikDuaLbl;
    }

    public JTextField getCodeTfl() {
        return codeTfl;
    }

    public void setCodeTfl(JTextField codeTfl) {
        this.codeTfl = codeTfl;
    }

    public JLabel getKegiatanLbl() {
        return kegiatanLbl;
    }

    public void setKegiatanLbl(JLabel kegiatanLbl) {
        this.kegiatanLbl = kegiatanLbl;
    }

    public JLabel getKegiatanTitikDuaLbl() {
        return kegiatanTitikDuaLbl;
    }

    public void setKegiatanTitikDuaLbl(JLabel kegiatanTitikDuaLbl) {
        this.kegiatanTitikDuaLbl = kegiatanTitikDuaLbl;
    }

    public JTextField getKegiatanTfl() {
        return kegiatanTfl;
    }

    public void setKegiatanTfl(JTextField kegiatanTfl) {
        this.kegiatanTfl = kegiatanTfl;
    }

    public JPanel getTopBottomPanel() {
        return topBottomPanel;
    }

    public void setTopBottomPanel(JPanel topBottomPanel) {
        this.topBottomPanel = topBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public JScrollPane getScrolPane() {
        return scrolPane;
    }

    public void setScrolPane(JScrollPane scrolPane) {
        this.scrolPane = scrolPane;
    }

    public JTable getKegiatanTable() {
        return kegiatanTable;
    }

    public void setKegiatanTable(JTable kegiatanTable) {
        this.kegiatanTable = kegiatanTable;
    }

    public DefaultTableModel getModelTableKegiatan() {
        return modelTableKegiatan;
    }

    public void setModelTableKegiatan(DefaultTableModel modelTableKegiatan) {
        this.modelTableKegiatan = modelTableKegiatan;
    }

    public List<Kegiatan> getListKegiatan() {
        return listKegiatan;
    }

    public void setListKegiatan(List<Kegiatan> listKegiatan) {
        this.listKegiatan = listKegiatan;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }

    public Kegiatan getSelectionKegiatan() {
        return selectionKegiatan;
    }

    public void setSelectionKegiatan(Kegiatan selectionKegiatan) {
        this.selectionKegiatan = selectionKegiatan;
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    

    
    
    
    
    
    
}
