/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.master;

import com.si.evkin.ctrl.sql.CtrlEvkin;
import com.si.evkin.model.master.Organisasi;
import com.si.evkin.model.master.Pemerintahan;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author TRI
 */
public class ViewOrganisasi extends JInternalFrame implements ActionListener, ListSelectionListener, ItemListener{

    private Connection conn;
    
    private JPanel topPanel = new JPanel();
    private JPanel bottomPanel = new JPanel();
    
    private JPanel topTopPanel = new JPanel();
    
    private JLabel pemerintahanLbl = new JLabel("Pemerintahan");
    private JLabel pemerintahanTitikDuaLbl = new JLabel(":");
    private JComboBox pemerintahanCbx = new JComboBox();
    private DefaultComboBoxModel modelComboboxPemerintahan = new DefaultComboBoxModel();
    private Pemerintahan selectionPemerintahan = new Pemerintahan();
    private List<Pemerintahan> listPemerintahan = new ArrayList<>();
    
    private JLabel codeLbl = new JLabel("Code");
    private JLabel codeTitikDuaLbl = new JLabel(":");
    private JTextField codeTfl = new JTextField();
    
    private JLabel organisasiLbl = new JLabel("Organisasi");
    private JLabel organisasiTitikDuaLbl = new JLabel(":");
    private JTextField organisasiTfl = new JTextField();
    
    
    private JPanel topBottomPanel = new JPanel();
    private JButton newBtn = new JButton("New");
    private JButton saveBtn = new JButton("Save");
    private JButton editBtn = new JButton("Edit");
    private JButton cancelBtn = new JButton("Cancel");
    private JButton deleteBtn = new JButton("Delete");   
    
    private JScrollPane scrolPane = new JScrollPane();
    private JTable organisasiTable = new JTable();
    private DefaultTableModel modelTableOrganisasi = new DefaultTableModel();
    private List<Organisasi> listOrganisasi = new ArrayList<>();   
    private int indexrow;   
    
    private CtrlEvkin ctrlEvkin = new CtrlEvkin();
    private Organisasi selectionOrganisasi = new Organisasi();
    private Boolean onNeworEdit;
    private long autoindex;
    
    
    
    public ViewOrganisasi(Connection conn){
        this.setTitle("Organisasi");
        this.setResizable(true);
        this.setClosable(true);      
        this.setIconifiable(true);
        this.setSize(500, 400);
        this.setResizable(false);
        this.setVisible(true);
        this.conn = conn;            
        this.component();        
        this.setDisableComponentOnStart();        
    }
    
    public void component(){
        
        topPanel.setLayout(new BorderLayout()); 
        topPanel.setBorder(new TitledBorder(new LineBorder(Color.BLACK, 2),"Input Organisasi")); 
        
        topTopPanel.setLayout(new GridBagLayout());    
        topTopPanel.setBorder(new LineBorder(Color.yellow));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;  
        gbc.insets = new Insets(5,5,5,5);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 0;
        topTopPanel.add(pemerintahanLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 0;
        topTopPanel.add(pemerintahanTitikDuaLbl, gbc);
        
        pemerintahanCbx.setModel(modelComboboxPemerintahan);
        pemerintahanCbx.addItemListener(this);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 0;
        topTopPanel.add(pemerintahanCbx, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 1;
        topTopPanel.add(codeLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 1;
        topTopPanel.add(codeTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 1;
        topTopPanel.add(codeTfl, gbc);
        
        gbc.weightx = 20.0; 
        gbc.gridx = 0;
        gbc.gridy = 2;
        topTopPanel.add(organisasiLbl, gbc);
        
        gbc.weightx = 5.0; 
        gbc.gridx = 1;
        gbc.gridy = 2;
        topTopPanel.add(organisasiTitikDuaLbl, gbc);
        
        gbc.weightx = 100.0; 
        gbc.gridx = 2;
        gbc.gridy = 2;
        topTopPanel.add(organisasiTfl, gbc);
                        
        topBottomPanel.setLayout(new FlowLayout());
        topBottomPanel.setBorder(new LineBorder(Color.GREEN));        
        topBottomPanel.add(newBtn);
        newBtn.addActionListener(this);
        topBottomPanel.add(saveBtn);
        saveBtn.addActionListener(this);
        topBottomPanel.add(editBtn);
        editBtn.addActionListener(this);
        topBottomPanel.add(cancelBtn);
        cancelBtn.addActionListener(this);
        topBottomPanel.add(deleteBtn);
        deleteBtn.addActionListener(this);
                               
        topPanel.add(topTopPanel, BorderLayout.NORTH);
        topPanel.add(topBottomPanel, BorderLayout.SOUTH);
        
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.setBorder(new TitledBorder(new LineBorder(Color.RED, 2),"Organisasi")); 
        
        modelTableOrganisasi = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column)
            {
              return false;
            }
        };                      
        organisasiTable = new JTable(modelTableOrganisasi);   
        organisasiTable.getSelectionModel().addListSelectionListener(this);       
        init();              
        organisasiTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        organisasiTable.setRowSelectionAllowed(true);
        organisasiTable.getColumnModel().getColumn(0).setMinWidth(0);
        organisasiTable.getColumnModel().getColumn(0).setMaxWidth(0);
        
        scrolPane = new JScrollPane(organisasiTable);
        bottomPanel.add(scrolPane);
                                                        
        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topPanel, bottomPanel);                                        
        
        this.add(spLeft);        
        
    }
    
    public void init(){
        try {
            
            modelTableOrganisasi.addColumn("autoindex");
            modelTableOrganisasi.addColumn("Pemerintahan");
            modelTableOrganisasi.addColumn("Code");
            modelTableOrganisasi.addColumn("Name");            
            
            Pemerintahan pemerintahan = new Pemerintahan();
            pemerintahan.setAutoindex(0);
            pemerintahan.setCode("");
            pemerintahan.setName("");
            
            listPemerintahan = ctrlEvkin.onLoadPemerintahan(conn); 
            modelComboboxPemerintahan.addElement(pemerintahan);
            for(Pemerintahan urs : listPemerintahan){
                modelComboboxPemerintahan.addElement(urs);
            }
                        
        } catch (Exception ex) {
            Logger.getLogger(ViewPegawai.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   
    
    public void setDisableComponentOnStart(){
        codeTfl.setText("");
        organisasiTfl.setText("");
        
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        organisasiTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    public void setDisableComponentOnSelection(){       
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        organisasiTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);             
    }
    
    public void setEnableComponentOnNew(){           
        codeTfl.setText("");
        organisasiTfl.setText("");
        
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        organisasiTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.TRUE;          
    }
    
    public void setEnableComponentOnSave(){                
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        organisasiTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(true);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(true);
        
    }
    
    public void setEnableComponentOnEdit(){ 
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(true);
        organisasiTfl.setEnabled(true);
        
        newBtn.setEnabled(false);
        saveBtn.setEnabled(true);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(true);
        deleteBtn.setEnabled(false);
        
        onNeworEdit = Boolean.FALSE;            
    }
    
    
    public void setEnableComponentOnCancel(){  
        if(autoindex!=0){
            codeTfl.setText(selectionOrganisasi.getCode());
            organisasiTfl.setText(selectionOrganisasi.getName());
           
            pemerintahanCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            organisasiTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(true);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(true);
        }else{
            codeTfl.setText("");
            organisasiTfl.setText("");

            pemerintahanCbx.setEnabled(true);
            codeTfl.setEnabled(false);
            organisasiTfl.setEnabled(false);

            newBtn.setEnabled(true);
            saveBtn.setEnabled(false);
            editBtn.setEnabled(false);
            cancelBtn.setEnabled(false);
            deleteBtn.setEnabled(false);
        }
                      
    }
    
    public void setEnableComponentOnDelete(){        
        codeTfl.setText("");
        organisasiTfl.setText("");
        
        pemerintahanCbx.setEnabled(true);
        codeTfl.setEnabled(false);
        organisasiTfl.setEnabled(false);
        
        newBtn.setEnabled(true);
        saveBtn.setEnabled(false);
        editBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        deleteBtn.setEnabled(false);             
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==newBtn){
            this.onNewClicked();    
        }else if(e.getSource()==saveBtn){
            this.onSaveCliked();   
        }else if(e.getSource()==editBtn){
            this.onEditClicked();
        }else if(e.getSource()==cancelBtn){
            this.onCancelClicked();    
        }else if(e.getSource()==deleteBtn){
            this.onDeleteClicked();    
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {        
        if (!e.getValueIsAdjusting()) {           
            try {
                indexrow = organisasiTable.getSelectedRow();
                autoindex= (long) organisasiTable.getModel().getValueAt(indexrow, 0);
                System.out.println("autoindex : "+autoindex);
                  
                selectionOrganisasi = ctrlEvkin.getOrganisasiByAutoindex(autoindex, conn);
              
                if(selectionOrganisasi!=null){
                    
                    System.out.println("autoindex : "+selectionOrganisasi.getAutoindex());
                    System.out.println("code : "+selectionOrganisasi.getCode());
                    System.out.println("name : "+selectionOrganisasi.getName());
                
                    
                    codeTfl.setText(selectionOrganisasi.getCode());
                    organisasiTfl.setText(selectionOrganisasi.getName());  
                    
                    setDisableComponentOnSelection();
                 }
                
            } catch (Exception ex) {
                Logger.getLogger(ViewOrganisasi.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
    }
    
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            try {
                Object item = e.getItem();
                System.out.println("itemStateChanged");
                selectionPemerintahan = (Pemerintahan)item;
                System.out.println("selectionPemerintahan : "+selectionPemerintahan.getAutoindex());
                
                this.removeTable();
                listOrganisasi = ctrlEvkin.onLoadOrganisasiByIndexPemerintahan(selectionPemerintahan.getAutoindex(), conn);
                for(Organisasi pmrth : listOrganisasi){
                    modelTableOrganisasi.addRow(new Object[]{pmrth.getAutoindex(), selectionPemerintahan.getName(), pmrth.getCode(), pmrth.getName()});
                }                  
            }catch (Exception ex) {
                Logger.getLogger(ViewOrganisasi.class.getName()).log(Level.SEVERE, null, ex);
            }          
        }
    }
    
    public void removeTable(){
        listOrganisasi.clear();
        DefaultTableModel dtm = (DefaultTableModel) organisasiTable.getModel();
        int rowCount = dtm.getRowCount();                
        for (int i = rowCount - 1; i >= 0; i--) {
            dtm.removeRow(i);
        }
    }
         
                    
    public void onNewClicked(){                   
        this.setEnableComponentOnNew();
    }
    
    public void onSaveCliked(){                      
        if(pemerintahanCbx.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(this, "PEMERINTAHAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(codeTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "KODE Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(organisasiTfl.getText().equals("")){
            JOptionPane.showMessageDialog(this, "PEMERINTAHAN Tidak Boleh Kosong", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            this.onSaveOrEdit();
            this.setEnableComponentOnSave();
        }          
    }
    
    public void onEditClicked(){
        this.setEnableComponentOnEdit();
    }
    
    public void onCancelClicked(){         
        this.setEnableComponentOnCancel();                           
    }
    
    public void onDeleteClicked(){
        this.onDelete();
        this.setEnableComponentOnDelete();
    }    
    
    public void onSaveOrEdit(){
        if(onNeworEdit){
            this.onSave();
        }else{
            this.onEdit();
        }
    }
    
    public void onSave(){ 
        try {
            Organisasi organisasi = new Organisasi();
            organisasi.setIndexPemerintahan(selectionPemerintahan.getAutoindex());
            organisasi.setCode(codeTfl.getText());
            organisasi.setName(organisasiTfl.getText());                               
            autoindex = ctrlEvkin.onSaveMasterOrganisasi(organisasi, conn);  
            organisasi.setAutoindex(autoindex);
            selectionOrganisasi = organisasi;
            
            this.removeTable();
            listOrganisasi = ctrlEvkin.onLoadOrganisasiByIndexPemerintahan(selectionPemerintahan.getAutoindex(), conn);
            for(Organisasi pmrth : listOrganisasi){
                modelTableOrganisasi.addRow(new Object[]{pmrth.getAutoindex(), selectionPemerintahan.getName(), pmrth.getCode(), pmrth.getName()});
            }            
            
        } catch (Exception ex) {
            Logger.getLogger(ViewOrganisasi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onEdit(){        
        try {           
            Organisasi organisasi = new Organisasi();
            organisasi.setAutoindex(selectionOrganisasi.getAutoindex());
            organisasi.setIndexPemerintahan(selectionPemerintahan.getAutoindex());
            organisasi.setCode(codeTfl.getText());
            organisasi.setName(organisasiTfl.getText());   
            selectionOrganisasi = organisasi;              
            ctrlEvkin.onUpdateMasterOrganisasi(organisasi, conn); 
            
            this.removeTable();
            listOrganisasi = ctrlEvkin.onLoadOrganisasiByIndexPemerintahan(selectionPemerintahan.getAutoindex(), conn);
            for(Organisasi pmrth : listOrganisasi){
                modelTableOrganisasi.addRow(new Object[]{pmrth.getAutoindex(), selectionPemerintahan.getName(), pmrth.getCode(), pmrth.getName()});
            }                        
        } catch (Exception ex) {
            Logger.getLogger(ViewOrganisasi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
            
    public void onDelete(){
        try {                 
            ctrlEvkin.onDeleteMasterOrganisasiByAutoindex(selectionOrganisasi.getAutoindex(), conn); 
            modelTableOrganisasi.removeRow(indexrow);
            indexrow = 0;
            autoindex = 0;
        } catch (Exception ex) {
            Logger.getLogger(ViewOrganisasi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }           
 
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public void setTopPanel(JPanel topPanel) {
        this.topPanel = topPanel;
    }

    public JPanel getBottomPanel() {
        return bottomPanel;
    }

    public void setBottomPanel(JPanel bottomPanel) {
        this.bottomPanel = bottomPanel;
    }

    public JPanel getTopTopPanel() {
        return topTopPanel;
    }

    public void setTopTopPanel(JPanel topTopPanel) {
        this.topTopPanel = topTopPanel;
    }

    public JLabel getCodeLbl() {
        return codeLbl;
    }

    public void setCodeLbl(JLabel codeLbl) {
        this.codeLbl = codeLbl;
    }

    public JLabel getCodeTitikDuaLbl() {
        return codeTitikDuaLbl;
    }

    public void setCodeTitikDuaLbl(JLabel codeTitikDuaLbl) {
        this.codeTitikDuaLbl = codeTitikDuaLbl;
    }

    public JTextField getCodeTfl() {
        return codeTfl;
    }

    public void setCodeTfl(JTextField codeTfl) {
        this.codeTfl = codeTfl;
    }

    public JLabel getOrganisasiLbl() {
        return organisasiLbl;
    }

    public void setOrganisasiLbl(JLabel organisasiLbl) {
        this.organisasiLbl = organisasiLbl;
    }

    public JLabel getOrganisasiTitikDuaLbl() {
        return organisasiTitikDuaLbl;
    }

    public void setOrganisasiTitikDuaLbl(JLabel organisasiTitikDuaLbl) {
        this.organisasiTitikDuaLbl = organisasiTitikDuaLbl;
    }

    public JTextField getOrganisasiTfl() {
        return organisasiTfl;
    }

    public void setOrganisasiTfl(JTextField organisasiTfl) {
        this.organisasiTfl = organisasiTfl;
    }

    public JPanel getTopBottomPanel() {
        return topBottomPanel;
    }

    public void setTopBottomPanel(JPanel topBottomPanel) {
        this.topBottomPanel = topBottomPanel;
    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public void setNewBtn(JButton newBtn) {
        this.newBtn = newBtn;
    }

    public JButton getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(JButton saveBtn) {
        this.saveBtn = saveBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public void setEditBtn(JButton editBtn) {
        this.editBtn = editBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(JButton deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public JScrollPane getScrolPane() {
        return scrolPane;
    }

    public void setScrolPane(JScrollPane scrolPane) {
        this.scrolPane = scrolPane;
    }

    public JTable getOrganisasiTable() {
        return organisasiTable;
    }

    public void setOrganisasiTable(JTable organisasiTable) {
        this.organisasiTable = organisasiTable;
    }

    public DefaultTableModel getModelTableOrganisasi() {
        return modelTableOrganisasi;
    }

    public void setModelTableOrganisasi(DefaultTableModel modelTableOrganisasi) {
        this.modelTableOrganisasi = modelTableOrganisasi;
    }

    public List<Organisasi> getListOrganisasi() {
        return listOrganisasi;
    }

    public void setListOrganisasi(List<Organisasi> listOrganisasi) {
        this.listOrganisasi = listOrganisasi;
    }

    public CtrlEvkin getCtrlEvkin() {
        return ctrlEvkin;
    }

    public void setCtrlEvkin(CtrlEvkin ctrlEvkin) {
        this.ctrlEvkin = ctrlEvkin;
    }

    public Organisasi getSelectionOrganisasi() {
        return selectionOrganisasi;
    }

    public void setSelectionOrganisasi(Organisasi selectionOrganisasi) {
        this.selectionOrganisasi = selectionOrganisasi;
    }

    public Boolean isOnNeworEdit() {
        return onNeworEdit;
    }

    public void setOnNeworEdit(Boolean onNeworEdit) {
        this.onNeworEdit = onNeworEdit;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    

    
    
    
    
    
    
}
