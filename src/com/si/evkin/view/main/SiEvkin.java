/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.evkin.view.main;

import com.si.evkin.ctrl.connection.ConnectionDB;
import com.si.evkin.view.master.ViewKegiatan;
import com.si.evkin.view.master.ViewOrganisasi;
import com.si.evkin.view.master.ViewPegawai;
import com.si.evkin.view.master.ViewPemerintahan;
import com.si.evkin.view.master.ViewProgram;
import com.si.evkin.view.master.ViewUrusan;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author TRI
 */
public final class SiEvkin extends JFrame implements ActionListener{
    
    public Connection conn = null;
    
    public JDesktopPane desktop = new JDesktopPane();
    
    public JMenuBar menuBar = new JMenuBar();
    
    public JMenu menuFile = new JMenu("File");
    public JMenuItem menuItemExit = new JMenuItem("Exit");
        
    public JMenu menuMaster = new JMenu("Master");
    public JMenuItem menuItemPegawai = new JMenuItem("Pegawai");
    public JMenuItem menuItemUrusan = new JMenuItem("Urusan");
    public JMenuItem menuItemPemerintahan = new JMenuItem("Pemerintahan");
    public JMenuItem menuItemOrganisasi = new JMenuItem("Organisasi");
    public JMenuItem menuItemProgram = new JMenuItem("Program");
    public JMenuItem menuItemKegiatan = new JMenuItem("Kegiatan");
        
    public JMenu menuDPA = new JMenu("DPA");
    public JMenuItem menuItemDPA = new JMenuItem("DPA");
    public JMenuItem menuItemDPA1 = new JMenuItem("DPA1");
    public JMenuItem menuItemDPA21 = new JMenuItem("DPA21");
    public JMenuItem menuItemDPA221 = new JMenuItem("DPA221");
    public JMenuItem menuItemDPA22 = new JMenuItem("DPA22");
    
    public JMenu menuLaporan = new JMenu("Laporan");
    public JMenuItem menuLaporanPerBulan = new JMenuItem("Laporan Per Bulan");

    
    public SiEvkin(){                                      
        this.component();
        this.connection();   
    }
    
    public void component(){
        
        this.setJMenuBar(menuBar);
        
        menuBar.add(menuFile);
        menuFile.add(menuItemExit);
        menuItemExit.addActionListener(this);
                
        menuBar.add(menuMaster);
        menuMaster.add(menuItemPegawai);
        menuMaster.add(menuItemUrusan);
        menuMaster.add(menuItemPemerintahan);
        menuMaster.add(menuItemOrganisasi);
        menuMaster.add(menuItemProgram);
        menuMaster.add(menuItemKegiatan);
        menuItemPegawai.addActionListener(this);
        menuItemUrusan.addActionListener(this);
        menuItemPemerintahan.addActionListener(this);
        menuItemOrganisasi.addActionListener(this);
        menuItemProgram.addActionListener(this);
        menuItemKegiatan.addActionListener(this);        
        
        menuBar.add(menuDPA);
        menuDPA.add(menuItemDPA);
        menuDPA.add(menuItemDPA1);
        menuDPA.add(menuItemDPA21);
        menuDPA.add(menuItemDPA221);
        menuDPA.add(menuItemDPA22);
        menuItemDPA.addActionListener(this);
        menuItemDPA1.addActionListener(this);
        menuItemDPA21.addActionListener(this);
        menuItemDPA221.addActionListener(this);
        menuItemDPA22.addActionListener(this);  
        
        menuBar.add(menuLaporan);
        menuLaporan.add(menuLaporanPerBulan);
        menuLaporanPerBulan.addActionListener(this);  
        
        this.setContentPane(desktop);
        
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SiEvkin siev = new SiEvkin();  
        siev.setTitle("Sistem Informasi Evaluasi Kinerja");   
        siev.setExtendedState(siev.getExtendedState() | siev.MAXIMIZED_BOTH);
        siev.setDefaultCloseOperation(EXIT_ON_CLOSE);
        siev.setVisible(true);         
    }
    
    
    public void connection(){
        ConnectionDB connDB = new ConnectionDB();
        conn = connDB.ConnectionDB();
        if(conn!=null){
            System.out.println("Connection Succes");
        }else{
            System.out.println("Connection Failed");
        }        
    } 

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==menuItemExit){
            System.exit(0);            
        }else if(e.getSource()==menuItemPegawai){
            System.out.println("menuItemPegawai");
            ViewPegawai vPegawai = new ViewPegawai(conn);
            desktop.add(vPegawai);                        
        }else if(e.getSource()==menuItemUrusan){
            System.out.println("menuItemUrusan");
            ViewUrusan vUrusan = new ViewUrusan(conn);
            desktop.add(vUrusan);
        }else if(e.getSource()==menuItemPemerintahan){
            System.out.println("menuItemPemerintahan");
            ViewPemerintahan vPemerintahan = new ViewPemerintahan(conn);
            desktop.add(vPemerintahan);
        }else if(e.getSource()==menuItemOrganisasi){
            System.out.println("menuItemOrganisasi");
            ViewOrganisasi vOrganisasi = new ViewOrganisasi(conn);
            desktop.add(vOrganisasi);
        }else if(e.getSource()==menuItemProgram){
            System.out.println("menuItemProgram");
            ViewProgram vProgram = new ViewProgram(conn);
            desktop.add(vProgram);
        }else if(e.getSource()==menuItemKegiatan){
            System.out.println("menuItemKegiatan");
            ViewKegiatan vKegiatan = new ViewKegiatan(conn);
            desktop.add(vKegiatan);
        }else if(e.getSource()==menuItemDPA){
            System.out.println("menuItemDPA");
        }else if(e.getSource()==menuItemDPA1){
            System.out.println("menuItemDPA1");
        }else if(e.getSource()==menuItemDPA21){
            System.out.println("menuItemDPA21");
        }else if(e.getSource()==menuItemDPA221){
            System.out.println("menuItemDPA221");
        }else if(e.getSource()==menuItemDPA22){
            System.out.println("menuItemDPA22");
        }else if(e.getSource()==menuLaporanPerBulan){
            System.out.println("menuLaporanPerBulan");
        }
    }    

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

}
